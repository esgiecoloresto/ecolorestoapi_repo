# Ecolo'Resto - Application Mobile

![Codeship](https://codeship.com/projects/8844a030-194f-0134-9562-4e04350ee748/status?branch=master)
![Heroku](https://heroku-badge.herokuapp.com/?app=ecolorestoapi)

Ecolo'Resto est un projet réalisé par Pierre BOUDON, Nicolas KERVOERN et Nicolas GIDON, dans le cadre du projet annuel de 3ème année en Ingénierie des Applications Mobiles (années 2015-2016) à l'ESGI.

#### Ce repository contient l'API du projet développée en NodeJS avec le framework Express.

#### Cette API est déployée en Intégration Continue avec l'outil Codeship et hébergée sur Heroku.

##### Le principe d'intégration continue est facilité grâce à l'utilisation de l'outil GitFlow qui permet de gérer facilement un projet Git de développement et notamment les releases sur la branche master.

Ce projet est décomposé en plusieurs repositories, chaque repository concernant une partie du projet :

  - Les documents importants :
    - Ecolo'RestoDocs_Repo
  - L'application Mobile (Swift) :
    - Ecolo'RestoMobileApp_Repo
  - L'application Desktop (JAVA) :
    - Ecolo'RestoDesktopApp_Repo
  - L'API (NodeJS/Express/MySQL/Codeship) :
    - Ecolo'RestoAPI_Repo

### Version
3.4.4
