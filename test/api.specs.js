var request = require('supertest')
var api = require('../index')

describe('Hello World', function() {
    it('should return Hello World', function() {
        return request(api)
            .get('/')
            .send()
            .expect("Hello World !")
            .expect(200)
    })
})

describe("[GET] routes", function() {
    describe("DISHES", function() {
        describe("/dishes", function() {
            describe("/(Latitude/Longitude) in Header", function() {
                it("No Latitude/Longitude - Should return all dishes", function() {
                    return request(api)
                        .get('/dishes')
                        .expect(200)
                })

                it("Bad Latitude/Longitude - Should return 10 dishes", function() {
                    return request(api)
                        .get('/dishes')
                        .set({ Latitude: "666541684651981561561891519151959155615161568456564565435.5123549875324523873543545384561651261856155", Longitude: "112651259846516849561591561531591561459161516514546545654.1565456889354515615915616873543543545" })
                        .expect(200)
                })

                it("String Latitude/Longitude - Should return 10 dishes", function() {
                    return request(api)
                        .get('/dishes')
                        .set({ Latitude: "Test", Longitude: "Test" })
                        .expect(200)
                })

                it("Good - Should return 10 closest dishes from the localisation", function() {
                    return request(api)
                        .get('/dishes')
                        .set({ Latitude: "48.8491485", Longitude: "2.3874704" })
                        .expect(200)
                })
            })

            describe("/pseudo", function() {
                it("pseudo - Should return all dishes of user", function() {
                    return request(api)
                        .get('/dishes/Pierre')
                        .expect(200)
                })

                it("pseudoSansPlat - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/dishes/User4')
                        .expect("Cet utilisateur n'a aucun plat.")
                        .expect(404)
                })

                it("pseudoInexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/dishes/pseudoInexistant')
                        .expect("Utilisateur non trouvé")
                        .expect(404)
                })
            })
        })
    })

    describe("USER", function() {
        describe("/users", function() {
            it("/ - Should return all users", function() {
                return request(api)
                    .get('/users')
                    .expect(200)
            })
        })

        describe("/user", function() {
            describe("/pseudo", function() {
                it("pseudoComplet - Should return user with complete informations", function() {
                    return request(api)
                        .get('/user/NicoK')
                        .expect(200)
                })

                it("pseudoSansPlat - Should return user without dishes", function() {
                    return request(api)
                        .get('/user/User4')
                        .expect(200)
                })

                it("pseudoSansAncienPlat - Should return user without old dishes", function() {
                    return request(api)
                        .get('/user/Pierre')
                        .expect(200)
                })

                it("pseudoSansRecette - Should return user without recipes", function() {
                    return request(api)
                        .get('/user/NicoGid')
                        .expect(200)
                })

                it("pseudoInexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/user/pseudoInexistant')
                        .expect("Aucun utilisateur trouvé")
                        .expect(404)
                })
            })

            describe("/(Pseudo/Password in Header)", function() {
                it("(PseudoComplet/Password) - Should return user with complete informations", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "NicoK", Password: "dc1ba8fb3f76dc768a2171fd2f455840b63c0a89667738a238643e5370895f5a" })
                        .expect(200)
                })

                it("(PseudoSansFavoris/Password) - Should return user without favorites", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "User5", Password: "0eeac8171768d0cdef3a20fee6db4362d019c91e10662a6b55186336e1a42778" })
                        .expect(200)
                })

                it("(PseudoSansAchat/Password) - Should return user without bought", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "NicoGid", Password: "37329c79764234c405a75b4d02047163bbc0f6cdeb761ad4a38a3953702b52b9" })
                        .expect(200)
                })

                it("(PseudoSansPlatsPassés/Password) - Should return user without old dishes", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "Pierre", Password: "25cad6463b5ea76e9341557b408b990741659036c39737867caf8c2965885005" })
                        .expect(200)
                })

                it("(PseudoSansRecette/Password) - Should return user without recipe", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "NicoGid", Password: "37329c79764234c405a75b4d02047163bbc0f6cdeb761ad4a38a3953702b52b9" })
                        .expect(200)
                })

                it("(PseudoSansPlat/Password) - Should return user without dishes", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "User4", Password: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                        .expect(200)
                })

                it("(pseudoInexistant/Password) - Should return 404 Not Foud error", function() {
                    return request(api)
                        .get('/user')
                        .set({ Pseudo: "pseudoInexistant", Password: "25cad6463b5ea76e9341557b408b990741659036c39737867caf8c2965885005" })
                        .expect("Aucun utilisateur trouvé")
                        .expect(404)
                })

                it("(Pseudo/passwordIncorrecte) - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/user')
                        .set({Pseudo: "Pierre", Password: "blabla"})
                        .expect("Aucun utilisateur trouvé")
                        .expect(404)
                })
            })

            describe("/favorites", function() {
                it("/pseudoExistantAvecFavoris - Should return favorites of user", function() {
                    return request(api)
                        .get('/user/favorites/Pierre')
                        .expect(200)
                })

                it("/pseudoExistantSansFavoris - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/user/favorites/User5')
                        .expect("Cet utilisateur n'a aucun favoris")
                        .expect(404)
                })

                it("/pseudoInexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .get('/user/favorites/pseudoInexistant')
                        .expect('Aucun utilisateur trouvé')
                        .expect(404)
                })
            })
        })
    })

    describe("RECIPES", function() {
        describe("/recipes", function() {
            it("/pseudoExistantAvecRecette - Should return addresses of user", function() {
                return request(api)
                    .get('/recipes/Pierre')
                    .expect(200)
            })

            it("/pseudoExistantSansRecette - Should return 404 Not Found error", function() {
                return request(api)
                    .get('/recipes/NicoGid')
                    .expect("Cet utilisateur n'a aucune recette")
                    .expect(404)
            })

            it("/pseudoInexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .get('/recipes/pseudoInexistant')
                    .expect('Aucun utilisateur trouvé')
                    .expect(404)
            })

            it("/ - Should return all recipes", function() {
                return request(api)
                    .get('/recipes')
                    .expect(200)
            })
        })
    })

    describe("DISH TRADES", function() {
        it("/trades - Should return all trades", function() {
            return request(api)
                .get('/trades')
                .expect(200)
        })
    })

    describe("COUNTERPARTIES", function() {
        it("/counterparties - Should return all counterparties", function() {
            return request(api)
                .get('/counterparties')
                .expect(200)
        })
    })

    describe("ADDRESSES", function() {
        describe("/addresses", function() {
            it("/pseudoExistant - Should return addresses of user", function() {
                return request(api)
                    .get('/addresses/Pierre')
                    .expect(200)
            })

            it("/pseudoInexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .get('/addresses/PseudoDUtilisateurInexistant')
                    .expect('Aucun utilisateur trouvé.')
                    .expect(404)
            })
        })
    })
})

describe("[POST] routes", function() {
    describe("USER", function() {
        describe("/user", function() {
            describe("/", function() {
                it("/ - Should create user", function() {
                    return request(api)
                        .post('/user')
                        .send({ email: "NouvelUtilisateur@email.com", pseudo: "NouvelUtilisateur", password: "nouvelUtilisateurMDP" })
                        .expect(200)
                })

                it("/ email existant - Should return 409 error", function() {
                    return request(api)
                        .post('/user')
                        .send({ email: "NouvelUtilisateur@email.com", pseudo: "NouveauPseudo", password: "nouvelUtilisateurMDP" })
                        .expect("Cette addresse email est déjà utilisée pour un autre compte")
                        .expect(409)
                })

                it("/ pseudoExistant - Should return 409 error", function() {
                    return request(api)
                        .post('/user')
                        .send({ email: "NouvelUtilisateurEmail@email.com", pseudo: "NouvelUtilisateur", password: "nouvelUtilisateurMDP" })
                        .expect("Ce pseudo est déjà utilisé")
                        .expect(409)
                })
            })

            describe("/favorite", function() {
                // Aucun utilisateur
                it("/pseudoInexistant/favorisExistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .post('/user/favorite/pseudoInexistant/NouvelUtilisateur')
                        .send()
                        .expect("Aucun utilisateur trouvé avec le pseudo")
                        .expect(404)
                })

                // Aucun utilisateur favoris
                it("/pseudoExistant/favorisInexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .post('/user/favorite/NouvelUtilisateur/pseudoInexistant')
                        .send()
                        .expect("Aucun utilisateur trouvé avec le pseudo du favoris")
                        .expect(404)
                })

                // Ajout dans les favoris
                it("/pseudoExistant/favorisExistant - Should add user in favorites", function() {
                    return request(api)
                        .post('/user/favorite/NouvelUtilisateur/Pierre')
                        .send()
                        .expect(200)
                })

                // Utilisateur déjà dans les favoris
                it("/pseudoExistant/favorisExistant - Should return 409 error", function() {
                    return request(api)
                        .post('/user/favorite/NouvelUtilisateur/Pierre')
                        .send()
                        .expect("Utilisateur déjà dans les favoris")
                        .expect(409)
                })
            })

            describe("/rating", function() {
                // Aucun utilisateur votant
                it("/ Aucun votant - Should return 400 error", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ to: "Pierre", note: 5 })
                        .expect("Pseudo du votant manquant")
                        .expect(400)
                })

                // Votant Inexistant
                it("/ Votant inexistant - Should return 404 error", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ rater: "votantInexistant", to: "Pierre", note: 5 })
                        .expect("Aucun utilisateur trouvé avec le pseudo du votant")
                        .expect(404)
                })

                // Aucun utilisateur voté
                it("/ Aucun voté - Should return 400 error", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ rater: "Pierre", note: 5 })
                        .expect("Pseudo du voté manquant")
                        .expect(400)
                })

                // Voté Inexistant
                it("/ Voté inexistant - Should return 404 error", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ rater: "Pierre", to: "votéInexistant", note: 5 })
                        .expect("Aucun utilisateur trouvé avec le pseudo du voté")
                        .expect(404)
                })

                // Aucune note
                it("/ Aucune note - Should return 400 error", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ rater: "NouvelUtilisateur", to: "Pierre" })
                        .expect("Note manquante")
                        .expect(400)
                })

                // Ajout de la note
                it("/ - Should add the note", function() {
                    return request(api)
                        .post('/user/rating')
                        .send({ rater: "NouvelUtilisateur", to: "Pierre", note: 5 })
                        .expect(200)
                })
            })
        })
    })

    describe("IMAGE", function() {
        describe("/image", function() {
            describe("/", function() {
                // Nom manquant
                it("/ Nom manquant - Should return 400 error", function() {
                    return request(api)
                        .post('/image')
                        .send({ link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true })
                        .expect("Nom manquant")
                        .expect(400)
                })

                // Lien manquant
                it("/ Lien manquant - Should return 400 error", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", isPrincipalImage: true })
                        .expect("Lien manquant")
                        .expect(400)
                })

                // Statut manquant
                it("/ Statut manquant - Should return 400 error", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg" })
                        .expect("Statut manquant")
                        .expect(400)
                })

                // Recette inexistante
                it("/ Recette inexistante - Should return 404 Not Found error", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true, RECIPE_id: 9999999999 })
                        .expect("Aucune recette trouvée pour cet ID")
                        .expect(404)
                })

                // Plat inexistant
                it("/ Plat inexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true, DISH_id: 9999999999 })
                        .expect("Aucun plat trouvée pour cet ID")
                        .expect(404)
                })

                // Création de l'image sans recette ni plat
                it("/ Création de l'image sans recette ni plat - Should create image", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true })
                        .expect(200)
                })

                // Création de l'image avec recette
                it("/ Création de l'image avec recette - Should create image and link it with recipe", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true, RECIPE_id: 11 })
                        .expect(200)
                })

                // Création de l'image avec plat
                it("/ Création de l'image avec recette - Should create image and link it with recipe", function() {
                    return request(api)
                        .post('/image')
                        .send({ name: "Sithachu", link: "https://res.cloudinary.com/teepublic/image/private/s--cJ-r-3pd--/t_Preview/b_rgb:262c3a,c_limit,f_jpg,h_630,q_90,w_630/v1446168211/production/designs/84395_0.jpg", isPrincipalImage: true, DISH_id: 81 })
                        .expect(200)
                })
            })
        })
    })

    describe("RECIPE", function() {
        describe("/recipe", function() {
            describe("/", function() {
                // Aucun ingrédients
                it("/ Aucun ingrédients - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "NicoK" })
                        .expect("Ingredients manquants")
                        .expect(400)
                })

                // Aucun nom
                it("/ Aucun nom - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "NicoK", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Nom manquant")
                        .expect(400)
                })

                // Aucune preparation
                it("/ Aucune preparation - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "NicoK", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Temps de preparation manquant")
                        .expect(400)
                })

                // Aucune cuisson
                it("/ Aucune cuisson - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "NicoK", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Temps de cuisson manquant")
                        .expect(400)
                })

                // Aucun nombre de couverts
                it("/ Aucun nombre de couverts - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "NicoK", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Nombre de personnes manquant")
                        .expect(400)
                })

                // Aucunes instructions
                it("/ Aucunes instructions - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "pseudo": "NicoK", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Instructions manquantes")
                        .expect(400)
                })

                // Aucun utilisateur
                it("/ Aucun utilisateur - Should return 400 error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("Utilisateur manquant")
                        .expect(400)
                })

                // Utilisateur inexistant
                it("/ Utilisateur inexistant - Should return 404 Not Found error", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "UtilisateurInexistant", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect("User inconnu")
                        .expect(404)
                })

                // Création de la recette
                it("/ - Should create recipe", function() {
                    return request(api)
                        .post('/recipe')
                        .send({ "name": "Tarte aux pommes et compote", "preparation": "15 min.", "baking": "40 min.", "serving": "4-6 pers.", "directions": "1 - Préchauffez le four th.6 (180°C).\r2 - Piquez la pâte déroulée dans le moule et faites-la cuire à blanc environ 10 minutes.\r3 - Pendant ce temps, épluchez les pommes et coupez-les en fins quartiers.\r4 - Sortez la pâte et étalez la compote, recouvrez de pommes, saupoudrez légèrement de cannelle et remettez le tout au four pendant 30 minutes.", "pseudo": "Pierre", "ingredients" : [ "1 rouleau de pâte feuilletée ou sablée", "3 belles pommes Golden", "2 pots de compote de pommes ou pomme-pêche", "Un peu de cannelle" ] })
                        .expect(200)
                })
            })
        })
    })

    describe("COUNTERPARTIES", function() {
        describe("/counterparties", function() {
            // Aucun tableau
            it("/ Aucun tableau - Should return 400 error", function() {
                return request(api)
                    .post("/counterparties")
                    .send()
                    .expect("Contreparties manquantes")
                    .expect(400)
            })

            // Tableau.length == 0
            it("/ Taille du tableau == 0 - Should return 400 error", function() {
                return request(api)
                    .post("/counterparties")
                    .send({ counterparties: [] })
                    .expect("Contreparties manquantes")
                    .expect(400)
            })

            // Ajout des contreparties
            it('/ Ajout des contreparties - Should add the counterparties', function() {
                return request(api)
                .post("/counterparties")
                .send({ "counterparties": [ { "name": "Livres", "priceIfPecuniary": null, "isFavoriteCounterParty": false, "TYPECOUNTERPARTY_id": 3, "DISH_id": 81 }, { "name": "Règlement en espèce", "priceIfPecuniary": 9, "isFavoriteCounterParty": true, "TYPECOUNTERPARTY_id": 1, "DISH_id": 81 } ] })
                .expect(200)
            })
        })
    })

    describe("TIMES", function() {
        describe("/times", function() {
            // Aucun tableau
            it("/ Aucun tableau - Should return 400 error", function() {
                return request(api)
                    .post("/times")
                    .send()
                    .expect("Temps manquants")
                    .expect(400)
            })

            // Tableau.length == 0
            it("/ Taille du tableau == 0 - Should return 400 error", function() {
                return request(api)
                    .post("/times")
                    .send({ times: [] })
                    .expect("Temps manquants")
                    .expect(400)
            })

            // Ajout des temps
            it('/ Ajout des temps - Should add the times', function() {
                return request(api)
                .post("/times")
                .send({ "times": [ { "date": "2016-06-05", "time": "17:30:00", "isDepositeDate": true, "isEndDate": false, "DISH_id": 81 }, { "date": "2016-06-05", "time": "21:30:00", "isDepositeDate": false, "isEndDate": true, "DISH_id": 81 }, { "date": "2016-06-05", "time": "19:00:00", "isDepositeDate": false, "isEndDate": false, "DISH_id": 81 } ] })
                .expect(200)
            })
        })
    })

    describe("DISH", function() {
        describe("/", function() {
            // Nom manquant
            it("/ Nom manquant - Should return 400 error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "dishDescription": "Description", "advices": "Advices", "pseudo": "Pierre", "ADDRESS_id": 12 })
                    .expect("Nom manquant")
                    .expect(400)
            })

            // Description manquante
            it("/ Description manquante - Should return 400 error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "advices": "Advices", "pseudo": "Pierre", "ADDRESS_id": 12 })
                    .expect("Description manquante")
                    .expect(400)
            })

            //  Conseils manquants
            it("/ Conseils manquants - Should return 400 error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "pseudo": "Pierre", "ADDRESS_id": 12 })
                    .expect("Conseils manquants")
                    .expect(400)
            })

            // Person manquants
            it("/ User manquants - Should return 400 error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "advices": "Advices", "ADDRESS_id": 12 })
                    .expect("User manquant")
                    .expect(400)
            })

            // Adresse manquante
            it("/ Adresse manquante - Should return 400 error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "advices": "Advices", "pseudo": "Pierre" })
                    .expect("Adresse manquante")
                    .expect(400)
            })

            // Utilisateur inexistant
            it("/ Utilisateur inexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "advices": "Advices", "pseudo": "UtilisateurInexistant", "ADDRESS_id": 12 })
                    .expect("Aucun utilisateur trouvé.")
                    .expect(404)
            })

            // Ajout du plat avec un idDISH
            it("/ Ajout du plat avec un idDISH - Should create a dish", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "advices": "Advices", "pseudo": "Pierre", "ADDRESS_id": 51 })
                    .expect(200)
            })

            // Ajout du plat avec une nouvelle adresse
            it("/ Ajout du plat avec une nouvelle adresse - Should create a dish", function() {
                return request(api)
                    .post('/dish')
                    .send({ "name": "Name", "dishDescription": "Description", "advices": "Advices", "pseudo": "NicoK", "firstName": "Testee", "lastName": "Testee", "address": "Testee", "addressComplement1": "Testee", "addressComplement2": "Testee", "zipCode": "54321", "city": "Testee" })
                    .expect(200)
            })
        })
    })
})

describe("[PUT] routes", function() {
    describe("DISH", function() {
        describe("/dish", function() {
            // Plat inexistant
            it("/state/:platInexistant/:pseudo/:customer - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/dish/state/Tartifloutte/Pierre/NicoK')
                    .send({ isInProgress: true, isReserved: false })
                    .expect("Dish not found")
                    .expect(404)
            })

            // Utilisateur inexistant
            it("/state/:plat/:pseudoInexistant/:customer - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/dish/state/Tartiflette/pseudoInexistant/NicoK')
                    .send({ isInProgress: true, isReserved: false })
                    .expect("User not found")
                    .expect(404)
            })

            // Client inexistant
            it("/state/:plat/:pseudo/:customerInexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/dish/state/Tartiflette/Pierre/UtilisateurInexistant')
                    .send({ isInProgress: true, isReserved: true })
                    .expect("Aucun utilisateur trouvé avec le pseudo du client")
                    .expect(404)
            })

            // Modification du plat
            it("/state/:plat/:pseudo/:customer - Should update dish state", function() {
                return request(api)
                    .put('/dish/state/Tartiflette/Pierre/NicoK')
                    .send({ isInProgress: false, isReserved: false })
                    .expect(200)
            })
        })
    })

    describe("USER", function() {
        describe("/user", function() {
            // Mot de passe invalide
            it("/:pseudo Mot de passe invalide - Should return 422 error", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4", newPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect("Mot de passe invalide")
                    .expect(422)
            })

            //Email invalide
            it("/:pseudo Email invalide - Should return 422 error", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74", newPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect("Email invalide")
                    .expect(422)
            })

            // Nouveau mot de passe invalide
            it("/:pseudo Nouveau mot de passe invalide - Should return 422 error", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4", oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect("Nouveau mot de passe invalide")
                    .expect(422)
            })

            // Pseudo ou mot de passe invalide
            it("/:pseudo Pseudo ou mot de passe invalide - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/user/User42')
                    .send({ email: "user@add.4", oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74", newPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect("Pseudo ou mot de passe invalide")
                    .expect(404)
            })

            // Modification de l'email de l'utilisateur
            it("/:pseudo Modification de l'email de l'utilisateur - Should update user's email", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4-2", oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74", newPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect(200)
            })

            // Modification du mot de passe de l'utilisateur
            it("/:pseudo Modification du mot de passe de l'utilisateur - Should update user's password", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4-2", oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74", newPassword: "5751f24cc1e9f7fcaa18a412f8fbf30c4e7b6a74e2e92ce666165fbfa9348104" })
                    .expect(200)
            })

            // Modification de l'email et du mot de passe de l'utilisateur
            it("/:pseudo Modification de l'email et du mot de passe de l'utilisateur - Should update user's email and password", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4", oldPassword: "5751f24cc1e9f7fcaa18a412f8fbf30c4e7b6a74e2e92ce666165fbfa9348104", newPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74" })
                    .expect(200)
            })

            // Modification de l'utilisateur avec une image
            it("/:pseudo Modification de l'utilisateur avec une image - Should update user's information AND image", function() {
                return request(api)
                    .put('/user/User4')
                    .send({ email: "user@add.4-2", oldPassword: "a417b5dc3d06d15d91c6687e27fc1705ebc56b3b2d813abe03066e5643fe4e74", newPassword: "5751f24cc1e9f7fcaa18a412f8fbf30c4e7b6a74e2e92ce666165fbfa9348104", image: 91 })
                    .expect(200)
            })
        })
    })

    describe('RECIPE', function() {
        describe("/recipe", function() {
            // Aucun utilisateur trouvé
            it("/:recipe/:pseudoInexistant/accept - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/recipe/Burger/UtilisateurInexistant/accept')
                    .send()
                    .expect("Aucun utilisateur trouvé")
                    .expect(404)
            })

            // Aucune recette trouvée
            it("/:recipeInexistante/:pseudo/accept - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/recipe/recetteInexistante/NicoK/accept')
                    .send()
                    .expect("Aucune recette trouvée ou la recette est peut-être déjà acceptée.")
                    .expect(404)
            })

            // Modification de la recette
            it("/:recipe/:pseudoInexistant/accept - Should return 404 Not Found error", function() {
                return request(api)
                    .put('/recipe/Burger/Pierre/accept')
                    .send()
                    .expect(200)
            })
        })
    })
})


describe("[DELETE] routes", function() {
    describe("RECIPES", function() {
        describe("/recipes", function() {
            // Aucun utilisateur
            it("/:name/:pseudoInexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .delete('/recipes/test/pseudoInexistant')
                    .send()
                    .expect("Aucun utilisateur trouvé")
                    .expect(404)
            })

            // Aucune recette trouvée
            it("/:nameInexistant/:pseudo - Should return 404 Not Found error", function() {
                return request(api)
                    .delete('/recipes/nameInexistant/Pierre')
                    .send()
                    .expect("Aucune recette trouvée")
                    .expect(404)
            })

            // Suppression de la recette
            it("/:nameInexistant/:pseudo - Should delete the recipe", function() {
                return request(api)
                    .delete('/recipes/test/Pierre')
                    .send()
                    .expect(200)
            })
        })
    })

    describe("USER", function() {
        describe("/user", function() {
            // Utilisateur non trouvé
            it("/:pseudoInexistant - Should return 404 Not Found error", function() {
                return request(api)
                    .delete('/user/pseudoInexistant')
                    .send()
                    .expect("Utilisateur non trouvé")
                    .expect(404)
            })

            // Suppression de l'utilisateur
            it("/:pseudo - Should delete the user", function() {
                return request(api)
                    .delete('/user/SimpleUser')
                    .send()
                    .expect(200)
            })
        })
    })
})
