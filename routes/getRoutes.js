var mysql = require('mysql')
var bodyparser = require('body-parser')
var Sequelize = require('sequelize')
var where = require('where')
var Perimeter = 5.0
var ENDPOINT = require('../ENDPOINT.js')

var sequelize = new Sequelize(ENDPOINT.endpoint, {
	define: {
		timestamps: false
	},
	logging: false
});

/* Déclaration Models */
var Address = sequelize.import("../models/address")
var Counterparty = sequelize.import("../models/counterparty")
var DishHasCounterparty = sequelize.import("../models/dish_has_counterparty")
var Dish = sequelize.import("../models/dish")
var Image = sequelize.import("../models/image")
var Ingredient = sequelize.import("../models/ingredient")
var PersonHasAddress = sequelize.import("../models/person_has_address")
var PersonHasNote = sequelize.import("../models/person_has_note")
var PersonHasFavorite = sequelize.import("../models/person_has_favorite")
var Person = sequelize.import("../models/person")
var Recipe = sequelize.import("../models/recipe")
var Time = sequelize.import("../models/time")
var TypeCounterparty = sequelize.import("../models/typecounterparty")

/* Declaration relations */
Address.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasOne(Address, { foreignKey: "DISH_id" })
Counterparty.belongsTo(TypeCounterparty, { foreignKey: "TYPECOUNTERPARTY_id" })
TypeCounterparty.hasMany(Counterparty, { foreignKey: "TYPECOUNTERPARTY_id" })

Dish.belongsTo(Person, { as: "auteur", foreignKey: "Creator_id" })
Person.hasMany(Dish, { as: "auteur", foreignKey: "Creator_id" })
Dish.belongsTo(Person, { as: "client", foreignKey: "Customer_id" })
Person.hasMany(Dish, { as: "client", foreignKey: "Customer_id" })

Dish.belongsToMany(Counterparty, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "DISH_id" })
Counterparty.belongsToMany(Dish, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "COUNTERPARTY_id" })

Image.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Image, { foreignKey: "DISH_id" })
Image.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Image, { foreignKey: "RECIPE_id" })

Ingredient.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Ingredient, { foreignKey: "RECIPE_id" })

Person.belongsTo(Image, { foreignKey: "IMAGE_id" })
Image.hasOne(Person, { foreignKey: "IMAGE_id" })

Person.belongsToMany(Person, { as: "idPERSONVotant", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotant" })
Person.belongsToMany(Person, { as: "idPERSONVotee", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotee" })

Person.belongsToMany(Address, { through: {model: PersonHasAddress, unique: false}, foreignKey: "PERSON_id" })
Address.belongsToMany(Person, { through: {model: PersonHasAddress, unique: false}, foreignKey: "ADDRESS_id" })

Person.belongsToMany(Person, { as: "favorites", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONLike" })
Person.belongsToMany(Person, { as: "likedBy", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONFavorite" })

Recipe.belongsTo(Person, { foreignKey: "PERSON_id" })
Person.hasMany(Recipe, { foreignKey: "PERSON_id" })
Time.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Time, { foreignKey: "DISH_id" })

module.exports = function(api){

	// Keep Hello World route for codeship
	api.get('/', function(req, res, next) {
		return res.status(200).send('Hello World !')
	})


	/* [GET] Routes */
	// DISHES - [localisation]
	api.get('/dishes', function(req, res, next) {
		var latitude = req.get("Latitude")
		var longitude = req.get("Longitude")

		if (latitude != undefined && longitude != undefined) {
			var dishesArround = []
			var otherDishes = []

			Dish.findAll({
				where: {
					isReserved: false
				},
				include: [
					{ model: Person, as: 'auteur', include: [ Image ], attributes: { exclude: ['password'] } },
					{ model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } },
					{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
					{ model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } },
					{ model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } }
				]
			}).then(function(dishes) {
				var asyncCallsRemaining = dishes.length

				dishes.forEach(function(dish) {
					var address = dish.address.address + " " + dish.address.zipCode + " " + dish.address.city

					var geocoder = new where.Geocoder;
					geocoder.toPoint({
						display_name: address
					}, function (err, points) {
						if (!err) {

							var distance = getDistanceFromLatLonInKm(latitude, longitude, points[0].lat, points[0].lon)
							dish["distance"] = distance

							if (distance <= Perimeter) {
								dishesArround.push(dish)
							} else {
								otherDishes.push(dish)
							}

						} else {
							console.log(err)
						}

						if (asyncCallsRemaining == 1) {

							if (dishesArround.length > 0) {

								sortedDishesArround = dishesArround.sort(function(a, b) {
									if (a.distance > b.distance)
										return 1
									if (a.distance < b.distance)
										return -1
									return 0
								})

								sortedDishesArround = sortedDishesArround.slice(0, 10)

								var asyncCallsRemaining2 = sortedDishesArround.length
								var completeDishesArround = []
								sortedDishesArround.forEach(function(dish) {
									dish = dish.toJSON()
									sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + dish.auteur.idPERSON).spread(function(results, metadata) {

										if (results[0]['moyenne'] && results[0]['voteCount']) {
											dish.auteur["note"] = results[0]['moyenne']
											dish.auteur["voteCount"] = results[0]['voteCount']
										} else {
											dish.auteur["note"] = -1
											dish.auteur["voteCount"] = -1
										}

										completeDishesArround.push(dish)

										if (asyncCallsRemaining2 == 1) {
											res.type('application/json')
											return res.status(200).send(prepareDishJSON(completeDishesArround))
										}

										asyncCallsRemaining2--;

									})
								})
							} else {

								sortedDishesArround = otherDishes.sort(function(a, b) {
									if (a.distance > b.distance)
										return 1
									if (a.distance < b.distance)
										return -1
									return 0
								})

								sortedDishesArround = sortedDishesArround.slice(0, 10)

								var asyncCallsRemaining2 = sortedDishesArround.length
								var completeDishesArround = []
								sortedDishesArround.forEach(function(dish) {
									dish = dish.toJSON()
									sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + dish.auteur.idPERSON).spread(function(results, metadata) {

										if (results[0]['moyenne'] && results[0]['voteCount']) {
											dish.auteur["note"] = results[0]['moyenne']
											dish.auteur["voteCount"] = results[0]['voteCount']
										} else {
											dish.auteur["note"] = -1
											dish.auteur["voteCount"] = -1
										}

										completeDishesArround.push(dish)

										if (asyncCallsRemaining2 == 1) {
											res.type('application/json')
											return res.status(200).send(prepareDishJSON(completeDishesArround))
										}

										asyncCallsRemaining2--;
									})
								})
							}
						}
						asyncCallsRemaining--;
					})
				})
			})
		} else {
			Dish.findAll({
				where: {
					isReserved: false
				},
				include: [
					{ model: Person, as: 'auteur', include: [ { model: Image } ], attributes: { exclude: ['password'] } },
					{ model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } },
					{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
					{ model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } },
					{ model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } }
				]
			}).then(function(dishes) {
				if (dishes && dishes.length > 0) {
					var completeDishes = []
					var asyncCallsRemaining = dishes.length
					dishes.forEach(function(dish) {
						dish = dish.toJSON()
						sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + dish.auteur.idPERSON).spread(function(results, metadata) {

							if (results[0]['moyenne'] && results[0]['voteCount']) {
								dish.auteur["note"] = results[0]['moyenne']
								dish.auteur["voteCount"] = results[0]['voteCount']
							} else {
								dish.auteur["note"] = -1
								dish.auteur["voteCount"] = -1
							}

							completeDishes.push(dish)

							if (asyncCallsRemaining == 1) {
								res.type('application/json')
								return res.status(200).send(prepareDishJSON(completeDishes))
							}

							asyncCallsRemaining--;
						})
					})
				} else {
					return res.status(404).send("Aucun plat")
				}
			})
		}
	})

	// DISHES - PSEUDO
	api.get('/dishes/:pseudo', function(req, res, next) {
		var pseudo = req.params.pseudo

		if (pseudo != "") {
			// Pseudo
			Person.find({
				where: {
					pseudo: pseudo
				}
			}).then(function(user) {
				if (user) {
					Dish.findAll({
						where: {
							Creator_id: user.idPERSON
						},
						include: [
							{ model: Person, as: 'auteur', include: [ Image ], attributes: { exclude: ['password'] } },
							{ model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } },
							{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
							{ model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } },
							{ model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } }
						]
					}).then(function(dishes) {
						if (dishes && dishes.length > 0) {
							var completeDishes = []
							var asyncCallsRemaining = dishes.length
							dishes.forEach(function(dish) {
								dish = dish.toJSON()
								sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + dish.auteur.idPERSON).spread(function(results, metadata) {

									if (results[0]['moyenne'] && results[0]['voteCount']) {
										dish.auteur["note"] = results[0]['moyenne']
										dish.auteur["voteCount"] = results[0]['voteCount']
									} else {
										dish.auteur["note"] = -1
										dish.auteur["voteCount"] = -1
									}

									completeDishes.push(dish)

									if (asyncCallsRemaining == 1) {
										res.type('application/json')
										return res.status(200).send(prepareDishJSON(completeDishes))
									}

									asyncCallsRemaining--;
								})
							})
						} else {
							return res.status(404).send("Cet utilisateur n'a aucun plat.")
						}
					})
				} else {
					return res.status(404).send("Utilisateur non trouvé");
				}
			})
		} else {
			return res.status(204).send("Pseudo invalide")
		}
	})

	// DISH STATE
	api.get('/dish/state/:plat/:pseudo', function(req, res, next) {
		var plat = req.params.plat
		var pseudo = req.params.pseudo

		if (pseudo != "" && plat != "") {
			Person.find({
				where: {
					pseudo: pseudo
				}
			}).then(function(user) {
				if (user) {
					Dish.find({
						attributes: ['isInProgress', 'isReserved'],
						where: {
							name: plat,
							Creator_id: user.idPERSON
						}
					}).then(function(dish) {
						if (dish) {
							res.type('application/json')
							return res.status(200).send(dish)
						} else {
							return res.status(404).send("Dish not found")
						}
					})
				} else {
					return res.status(404).send("User not found")
				}
			})
		} else {
			return res.status(204).send("Pseudo ou plat invalide")
		}
	})

	// USER
	api.get('/users', function(req, res, next) {
		Person.findAll({
			attributes: {
				exclude: 'password'
			},
			include: [
				{ model: Image }
			]
		}).then(function(users) {
			if (users) {
				var completedUsers = []
				var asyncCallsRemaining = users.length

				users.forEach(function(user) {
					user = user.toJSON()
					sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + user.idPERSON).spread(function(results, metadata) {

						if (results[0]['moyenne'] && results[0]['voteCount']) {
							user["note"] = results[0]['moyenne']
							user["voteCount"] = results[0]['voteCount']
						} else {
							user["note"] = -1
							user["voteCount"] = -1
						}

						completedUsers.push(user)

						if (asyncCallsRemaining == 1) {
							res.type('application/json')
							return res.status(200).send(completedUsers)
						}

						asyncCallsRemaining--;
					})
				})

				/*res.type('application/json')
				return res.status(200).send(JSON.stringify(users))*/
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

	// DISH FROM PSEUDO
	api.get('/user/:pseudo', function(req, res, next) {
		var pseudo = req.params.pseudo

		var oldDishes = []
		var currentDishes = []

		Person.find({
			attributes: {
				exclude: ['password']
			},
			where: {
				pseudo: pseudo
			},
			include:  [
				{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
				/*{ model: Recipe, attributes: { exclude: ['idRECIPE', 'PERSON_id'] }, include: [ { model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] }, where: {isPrincipalImage: true} } ] },*/
			]
		}).then(function(user) {
			if (user) {
				user = user.toJSON()
				Dish.findAll({
					attributes: {
						exclude: ['idDISH']
					},
					where: {
						Creator_id: user.idPERSON
					},
					include:  [
						{ model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } },
						{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
						{ model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } },
						{ model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } }
					]
				}).then(function(dishes) {

					if (dishes) {
						dishes.forEach(function(dish) {
							if (dish.isReserved || dish.isInProgress) {
								oldDishes.push(dish)
							} else {
								currentDishes.push(dish)
							}
						})

						user["currentDishes"] = currentDishes
						user["oldDishes"] = oldDishes
					} else {
						user["currentDishes"] = currentDishes
						user["oldDishes"] = oldDishes
					}

					Recipe.findAll({
						attributes: {
							exclude: ['idRECIPE', 'PERSON_id']
						},
						where: {
							PERSON_id: user.idPERSON,
							isAccepted: true
						},
						include: [
							{ model: Ingredient, attributes: { exclude: ['idINGREDIENT', 'RECIPE_id'] } },
							{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] }, where: {isPrincipalImage: true} },
						]
					}).then(function(recipes) {
						if (recipes && recipes.length > 0) {
							user["recipes"] = recipes
						} else {
							user["recipes"] = []
						}

						sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + user.idPERSON).spread(function(results, metadata) {

							if (results[0]['moyenne'] && results[0]['voteCount']) {
								user["note"] = results[0]['moyenne']
								user["voteCount"] = results[0]['voteCount']
							} else {
								user["note"] = -1
								user["voteCount"] = -1
							}

							delete user.idPERSON
							return res.status(200).send(user)
						})
					})
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

	// USER ACCOUNT
	api.get('/user', function(req, res, next) {
		/*var email = req.get("Email")*/
		var pseudo = req.get("Pseudo")
		var password = req.get("Password")

		var oldDishes = []
		var currentDishes = []

		Person.find({
			attributes: {
				exclude: ['password']
			},
			where: {
				/*email: email,*/
				pseudo: pseudo,
				password: password
			},
			include:  [
				{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
				/*{ model: Recipe, attributes: { exclude: ['idRECIPE', 'PERSON_id'] }, include: [ { model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] }, where: {isPrincipalImage: true} } ] },*/
				{ model: Person, as: 'favorites', attributes: { exclude: ['password'] }, include: [ { model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } } ] } ,
				{ model: Dish, as: 'client', attributes: { exclude: ['idDISH'] }, include:  [{ model: Person, as: "auteur", attributes: { exclude: ['password'] }, include: [{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } }] }, { model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } }, { model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } }, { model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } }, { model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } } ] },
			]
		}).then(function(user) {
			if (user) {
				user = user.toJSON()
				Dish.findAll({
					attributes: {
						exclude: ['idDISH']
					},
					where: {
						Creator_id: user.idPERSON
					},
					include:  [
						{ model: Address, attributes: { exclude: ['idADDRESS', 'DISH_id'] } },
						{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
						{ model: Counterparty, attributes: { exclude: ['idCOUNTERPARTY', 'TYPECOUNTERPARTY_id', 'dish_has_counterparty'] } },
						{ model: Time, attributes: { exclude: ['idTIME', 'DISH_id'] } }
					]
				}).then(function(dishes) {

					if (dishes && dishes.length > 0) {
						dishes.forEach(function(dish) {
							if (dish.isReserved || dish.isInProgress) {
								oldDishes.push(dish)
							} else {
								currentDishes.push(dish)
							}
						})

						user["currentDishes"] = currentDishes
						user["oldDishes"] = oldDishes
					} else {
						user["currentDishes"] = []
						user["oldDishes"] = []
					}

					Recipe.findAll({
						attributes: {
							exclude: ['idRECIPE', 'PERSON_id']
						},
						where: {
							PERSON_id: user.idPERSON,
							isAccepted: true
						},
						include: [
							{ model: Ingredient, attributes: { exclude: ['idINGREDIENT', 'RECIPE_id'] } },
							{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] }, where: {isPrincipalImage: true} },
						]
					}).then(function(recipes) {
						if (recipes && recipes.length > 0) {
							user["recipes"] = recipes
						} else {
							user["recipes"] = []
						}

						sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + user.idPERSON).spread(function(results, metadata) {

							if (results[0]['moyenne'] && results[0]['voteCount']) {
								user["note"] = results[0]['moyenne']
								user["voteCount"] = results[0]['voteCount']
							} else {
								user["note"] = -1
								user["voteCount"] = -1
							}

							delete user.idPERSON

							if (user.client.length > 0) {
								var boughtDishesAsyncCallsRemaining = user.client.length
								user.client.forEach(function(boughtDish) {
									sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + boughtDish.auteur.idPERSON).spread(function(results, metadata) {
										if (results[0]['moyenne'] && results[0]['voteCount']) {
											boughtDish.auteur["note"] = results[0]['moyenne']
											boughtDish.auteur["voteCount"] = results[0]['voteCount']
										} else {
											boughtDish.auteur["note"] = -1
											boughtDish.auteur["voteCount"] = -1
										}

										if (boughtDishesAsyncCallsRemaining == 1) {

											if (user.favorites.length > 0) {
												var favoritesAsyncCallsRemaining = user.favorites.length
												user.favorites.forEach(function(favorite) {
													sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + favorite.idPERSON).spread(function(results, metadata) {

														if (results[0]['moyenne'] && results[0]['voteCount']) {
															favorite["note"] = results[0]['moyenne']
															favorite["voteCount"] = results[0]['voteCount']
														} else {
															favorite["note"] = -1
															favorite["voteCount"] = -1
														}

														if (favoritesAsyncCallsRemaining == 1) {
															return res.status(200).send(user)
														}

														favoritesAsyncCallsRemaining--
													})
												})
											} else {
												return res.status(200).send(user)
											}

										}

										boughtDishesAsyncCallsRemaining--
									})
								})
							} else {
								if (user.favorites.length > 0) {
									var favoritesAsyncCallsRemaining = user.favorites.length
									user.favorites.forEach(function(favorite) {
										sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + favorite.idPERSON).spread(function(results, metadata) {

											if (results[0]['moyenne'] && results[0]['voteCount']) {
												favorite["note"] = results[0]['moyenne']
												favorite["voteCount"] = results[0]['voteCount']
											} else {
												favorite["note"] = -1
												favorite["voteCount"] = -1
											}

											if (favoritesAsyncCallsRemaining == 1) {
												return res.status(200).send(user)
											}

											favoritesAsyncCallsRemaining--
										})
									})
								} else {
									return res.status(200).send(user)
								}
							}
						})
					})
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

	// USER FAVORITES
	api.get('/user/favorites/:pseudo', function(req, res, next) {
		var pseudo = req.params.pseudo

		Person.find({
			attributes: {
				exclude: ['password']
			},
			where: {
				pseudo: pseudo
			},
			include: [
				{ model: Person, as:'favorites', attributes: [ 'pseudo', 'IMAGE_id' ], include: { model: Image, attributes: [ 'link' ] } }
			]
		}).then(function(person) {
			if (person) {
				var favorites = []

				person.favorites.forEach(function(favorite) {
					favorite = favorite.toJSON()

					favorite.image = favorite.image.link

					delete favorite.person_has_favorite

					favorites.push(favorite)
				})

				if (favorites.length > 0) {
					res.type('application/json')
					return res.status(200).send(favorites)
				} else
					return res.status(404).send("Cet utilisateur n'a aucun favoris")

			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

	// RECIPE
	api.get('/recipes/:pseudo', function(req, res, next) {
		var pseudo = req.params.pseudo

		Person.find({
			attributes: {
				exclude: ['password']
			},
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Recipe.findAll({
					attributes: {
						exclude: ['idRECIPE', 'PERSON_id']
					},
					where: {
						PERSON_id: user.idPERSON
					},
					include: [
						{ model: Image, attributes: { exclude: ['idIMAGE', 'RECIPE_id', 'DISH_id'] } },
						{ model: Person, include: [ Image ], attributes: { exclude: ['password'] } },
						{ model: Ingredient, attributes: { exclude: ['idINGREDIENT', 'RECIPE_id'] } }
					]
				}).then(function(recipes) {
					if (recipes && recipes.length > 0) {
						var asyncCallsRemaining = recipes.length
						var completedRecipes = []
						recipes.forEach(function(recipe) {
							recipe = recipe.toJSON()

							recipe["auteur"] = recipe.person
							delete recipe.person
							recipe.auteur.image = recipe.auteur.image.link


							sequelize.query("SELECT COUNT(*) as 'voteCount', ROUND(AVG(note)) as 'moyenne' FROM person_has_note WHERE idPERSONVotee = " + recipe.auteur.idPERSON).spread(function(results, metadata) {

								if (results[0]['moyenne'] && results[0]['voteCount']) {
									recipe.auteur["note"] = results[0]['moyenne']
									recipe.auteur["voteCount"] = results[0]['voteCount']
								} else {
									recipe.auteur["note"] = -1
									recipe.auteur["voteCount"] = -1
								}

								delete recipe.auteur.idPERSON

								if (asyncCallsRemaining == 1) {
									res.type('application/json')
									return res.status(200).send(completedRecipes)
								}

								asyncCallsRemaining--;
							})

							completedRecipes.push(recipe)
						})
					} else {
						return res.status(404).send("Cet utilisateur n'a aucune recette")
					}
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

	// recipe all
	api.get('/recipes', function(req, res, next) {
		Recipe.findAll({
			attributes: {
				exclude: ['idRECIPE', 'PERSON_id']
			},
			include: [
				{ model: Image },
				{ model: Person, include: [ Image ], attributes: { exclude: ['password'] } },
				{ model: Ingredient, attributes: { exclude: ['idINGREDIENT', 'RECIPE_id'] } }
			]
		}).then(function(recipes) {
			if (recipes && recipes.length > 0) {
				res.type('application/json')
				return res.status(200).send(recipes)
			} else {
				return res.status(404).send("Aucune recette.")
			}
		})
	})

	// DISH TRADES
	api.get('/trades', function(req, res, next) {
		Dish.findAll({
			where: {
				isReserved: true,
				Customer_id: {
					$ne: null
				}
			},
			include: [
				{ model: Person, as: 'auteur', include: [ { model: Image } ], attributes: { exclude: ['password'] } },
				{ model: Person, as: 'client', include: [ { model: Image } ], attributes: { exclude: ['password'] } }
			]
		}).then(function(dishes) {
			if (dishes && dishes.length > 0) {
				var asyncCallsRemaining = dishes.length
				var completeDishes = []
				dishes.forEach(function(dish) {
					var dish = dish.toJSON()

					dish.auteur.image = dish.auteur.image.link
					dish.client.image = dish.client.image.link

					completeDishes.push(dish)

					if (asyncCallsRemaining == 1) {
						res.type('application/json')
						return res.status(200).send(completeDishes)
					}
					asyncCallsRemaining--
				})
			} else {
				return res.status(404).send("Aucun plat")
			}
		})
	})

	// COUNTERPARTIES
	api.get('/counterparties', function(req, res, next) {
		array = {}
		Counterparty.findAll({
			attributes: {
				exclude: ['TYPECOUNTERPARTY_id']
			},
			include: [
				{ model: TypeCounterparty }
			]
		}).then(function(counterparties) {
			if (counterparties && counterparties.length > 0) {
				array["counterparties"] = counterparties

				TypeCounterparty.findAll().then(function(typesCounterparties) {
					if (typesCounterparties && typesCounterparties.length > 0) {
						array["typesCounterparties"] = typesCounterparties

						res.type('application/json')
						return res.status(200).send(array)
					} else {
						return res.status(404).send("Aucun type de contrepartie trouvé.")
					}
				})
			} else {
				return res.status(404).send("Aucune contrepartie trouvée.")
			}
		})
	})

	// ADDRESSES
	api.get('/addresses/:pseudo', function(req, res, next) {
		Person.find({
			attributes: ['pseudo'],
			where: {
				pseudo: req.params.pseudo
			},
			include: [
				{ model: Address }
			]
		}).then(function(user) {
			if (user) {
				res.type('application/json')
				return res.status(200).send(user)
			} else {
				return res.status(404).send("Aucun utilisateur trouvé.")
			}
		})
	})
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1);  // deg2rad below
	var dLon = deg2rad(lon2-lon1);
	var a =
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
		Math.sin(dLon/2) * Math.sin(dLon/2)
	;

	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c; // Distance in km

	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}

function prepareDishJSON(dishes) {
	var newDishes = []

	dishes.forEach(function(dish) {
		// Do modifications
		// -- Delete items
		delete dish.auteur.idPERSON
		dish.counterparties.forEach(function(counterparty) {
			delete dish.idDISH
			delete counterparty.dish_has_counterparty

			if (counterparty.priceIfPecuniary != null)
				counterparty['isPecuniary'] = true
			else
				counterparty['isPecuniary'] = false
		})

		// -- Replace information
		dish.auteur.image = dish.auteur.image.link


		// Add it to array
		newDishes.push(dish)
	})

	return newDishes
}
