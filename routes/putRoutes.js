var mysql = require('mysql')
var bodyparser = require('body-parser')
var Sequelize = require('sequelize')
var ENDPOINT = require('../ENDPOINT.js')

var sequelize = new Sequelize(ENDPOINT.endpoint, {
	define: {
		timestamps: false
	},
	logging: false
});

/* Déclaration Models */
var Address = sequelize.import("../models/address")
var Counterparty = sequelize.import("../models/counterparty")
var DishHasCounterparty = sequelize.import("../models/dish_has_counterparty")
var Dish = sequelize.import("../models/dish")
var Image = sequelize.import("../models/image")
var Ingredient = sequelize.import("../models/ingredient")
var PersonHasAddress = sequelize.import("../models/person_has_address")
var PersonHasNote = sequelize.import("../models/person_has_note")
var PersonHasFavorite = sequelize.import("../models/person_has_favorite")
var Person = sequelize.import("../models/person")
var Recipe = sequelize.import("../models/recipe")
var Time = sequelize.import("../models/time")
var TypeCounterparty = sequelize.import("../models/typecounterparty")

/* Declaration relations */
Address.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasOne(Address, { foreignKey: "DISH_id" })
Counterparty.belongsTo(TypeCounterparty, { foreignKey: "TYPECOUNTERPARTY_id" })
TypeCounterparty.hasMany(Counterparty, { foreignKey: "TYPECOUNTERPARTY_id" })

Dish.belongsTo(Person, { as: "auteur", foreignKey: "Creator_id" })
Person.hasMany(Dish, { as: "auteur", foreignKey: "Creator_id" })
Dish.belongsTo(Person, { as: "client", foreignKey: "Customer_id" })
Person.hasMany(Dish, { as: "client", foreignKey: "Customer_id" })

Dish.belongsToMany(Counterparty, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "DISH_id" })
Counterparty.belongsToMany(Dish, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "COUNTERPARTY_id" })

Image.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Image, { foreignKey: "DISH_id" })
Image.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Image, { foreignKey: "RECIPE_id" })

Ingredient.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Ingredient, { foreignKey: "RECIPE_id" })

Person.belongsTo(Image, { foreignKey: "IMAGE_id" })
Image.hasOne(Person, { foreignKey: "IMAGE_id" })

Person.belongsToMany(Person, { as: "idPERSONVotant", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotant" })
Person.belongsToMany(Person, { as: "idPERSONVotee", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotee" })

Person.belongsToMany(Address, { through: {model: PersonHasAddress, unique: false}, foreignKey: "PERSON_id" })
Address.belongsToMany(Person, { through: {model: PersonHasAddress, unique: false}, foreignKey: "ADDRESS_id" })

Person.belongsToMany(Person, { as: "favorites", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONLike" })
Person.belongsToMany(Person, { as: "likedBy", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONFavorite" })

Recipe.belongsTo(Person, { foreignKey: "PERSON_id" })
Person.hasMany(Recipe, { foreignKey: "PERSON_id" })
Time.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Time, { foreignKey: "DISH_id" })

module.exports = function(api){

	api.put('/dish/state/:plat/:pseudo/:customer', function(req, res, next) {
		var plat = req.params.plat
		var pseudo = req.params.pseudo
		var customerPseudo = req.params.customer

		var isInProgress = req.body.isInProgress
		var isReserved = req.body.isReserved

		if (!pseudo || pseudo == "") {
			return res.status(422).send("Pseudo invalide")
		}

		if (!plat || plat == "") {
			return res.status(422).send("Plat invalide")
		}

		Person.find({
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Dish.find({
					where: {
						name: plat,
						Creator_id: user.idPERSON
					}
				}).then(function(dish) {
					if (dish) {

						dish.update({ isInProgress: isInProgress, isReserved: isReserved }, {fields: ['isInProgress', 'isReserved']}).then(function(dish) {
							if (dish) {
								if (dish.isInProgress && dish.isReserved) {
									Person.find({
										attributes: ['idPERSON'],
										where: {
											pseudo: customerPseudo
										}
									}).then(function(customer) {
										if (customer) {
											dish.update({ Customer_id: customer.idPERSON }, {fields: ['Customer_id']}).then(function(completeDish) {
												if (completeDish) {
													return res.status(200).send(completeDish)
												} else {
													return res.status(503).send("Impossible d'ajouter le client au plat.")
												}
											})
										} else {
											return res.status(404).send("Aucun utilisateur trouvé avec le pseudo du client")
										}
									})
								} else {
									return res.status(200).send(dish)
								}
							} else {
								return res.status(503).send("Impossible de modifier le plat")
							}
						})
					} else {
						return res.status(404).send("Dish not found")
					}
				})
			} else {
				return res.status(404).send("User not found")
			}
		})
	})

	api.put('/user/:pseudo', function(req, res, next) {
		var pseudo = req.params.pseudo
		var oldPassword = req.body.oldPassword
		var email = req.body.email
		var newPassword = req.body.newPassword
		var image = req.body.image

		if (!pseudo || pseudo == "")
			return res.status(422).send("Pseudo invalide")

		if (!oldPassword || oldPassword == "")
			return res.status(422).send("Mot de passe invalide")

		if (!email || email == "")
			return res.status(422).send("Email invalide")

		if (!newPassword || newPassword == "")
			return res.status(422).send("Nouveau mot de passe invalide")

		if (!image || image == null)
			image = 1


		Image.find({
			where : {
				idIMAGE: image
			}
		}).then(function(image) {
			if (!image) {
				return res.status(404).send("Aucune image n'existe pour cet ID")
			} else {
				Person.find({
					attributes: {
						exclude: ['password']
					},
					where: {
						pseudo: pseudo,
						password: oldPassword
					}
				}).then(function(user) {
					if (user) {
						user.update({ email: email, password: newPassword, IMAGE_id: image.idIMAGE }, {fields: ['email', 'password', 'IMAGE_id']}).then(function(user) {
							if (user) {
								user = user.toJSON()
								user["message"] = "Utilisateur correctement modifié"
								return res.status(200).send(user)
							} else {
								return res.status(503).send("Impossible de modifier l'Utilisateur")
							}
						})
					} else {
						return res.status(404).send("Pseudo ou mot de passe invalide")
					}
				})
			}
		})
	})

	api.put('/recipe/:recipe/:pseudo/accept', function(req, res, next) {
		var pseudo = req.params.pseudo
		var recipe = req.params.recipe

		if (!pseudo || pseudo == "") {
			return res.status(422).send("Pseudo invalide")
		}

		if (!recipe || recipe == "") {
			return res.status(422).send("Recette invalide")
		}

		Person.find({
			attributes: ['idPERSON'],
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Recipe.find({
					where: {
						name: recipe,
						PERSON_id: user.idPERSON,
						isAccepted: false
					}
				}).then(function(recipe) {
					if (recipe) {
						recipe.update({ isAccepted: true }, { fields: ['isAccepted'] }).then(function(recipe) {
							if (recipe) {
								recipe = recipe.toJSON()
								recipe["message"] = "Recette acceptée"
								return res.status(200).send(recipe)
							} else {
								return res.status(503).send("Impossible de modifier la recette")
							}
						})
					} else {
						return res.status(404).send("Aucune recette trouvée ou la recette est peut-être déjà acceptée.")
					}
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})

}
