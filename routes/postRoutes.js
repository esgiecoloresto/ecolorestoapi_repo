var mysql = require('mysql')
var bodyparser = require('body-parser')
var Sequelize = require('sequelize')
var ENDPOINT = require('../ENDPOINT.js')

var sequelize = new Sequelize(ENDPOINT.endpoint, {
	define: {
		timestamps: false
	},
	logging: false
});

/* Déclaration Models */
var Address = sequelize.import("../models/address")
var Counterparty = sequelize.import("../models/counterparty")
var DishHasCounterparty = sequelize.import("../models/dish_has_counterparty")
var Dish = sequelize.import("../models/dish")
var Image = sequelize.import("../models/image")
var Ingredient = sequelize.import("../models/ingredient")
var PersonHasAddress = sequelize.import("../models/person_has_address")
var PersonHasNote = sequelize.import("../models/person_has_note")
var PersonHasFavorite = sequelize.import("../models/person_has_favorite")
var Person = sequelize.import("../models/person")
var Recipe = sequelize.import("../models/recipe")
var Time = sequelize.import("../models/time")
var TypeCounterparty = sequelize.import("../models/typecounterparty")

/* Declaration relations */
Address.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasOne(Address, { foreignKey: "DISH_id" })
Counterparty.belongsTo(TypeCounterparty, { foreignKey: "TYPECOUNTERPARTY_id" })
TypeCounterparty.hasMany(Counterparty, { foreignKey: "TYPECOUNTERPARTY_id" })

Dish.belongsTo(Person, { as: "auteur", foreignKey: "Creator_id" })
Person.hasMany(Dish, { as: "auteur", foreignKey: "Creator_id" })
Dish.belongsTo(Person, { as: "client", foreignKey: "Customer_id" })
Person.hasMany(Dish, { as: "client", foreignKey: "Customer_id" })

Dish.belongsToMany(Counterparty, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "DISH_id" })
Counterparty.belongsToMany(Dish, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "COUNTERPARTY_id" })

Image.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Image, { foreignKey: "DISH_id" })
Image.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Image, { foreignKey: "RECIPE_id" })

Ingredient.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Ingredient, { foreignKey: "RECIPE_id" })

Person.belongsTo(Image, { foreignKey: "IMAGE_id" })
Image.hasOne(Person, { foreignKey: "IMAGE_id" })

Person.belongsToMany(Person, { as: "idPERSONVotant", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotant" })
Person.belongsToMany(Person, { as: "idPERSONVotee", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotee" })

Person.belongsToMany(Address, { through: {model: PersonHasAddress, unique: false}, foreignKey: "PERSON_id" })
Address.belongsToMany(Person, { through: {model: PersonHasAddress, unique: false}, foreignKey: "ADDRESS_id" })

Person.belongsToMany(Person, { as: "favorites", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONLike" })
Person.belongsToMany(Person, { as: "likedBy", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONFavorite" })

Recipe.belongsTo(Person, { foreignKey: "PERSON_id" })
Person.hasMany(Recipe, { foreignKey: "PERSON_id" })
Time.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Time, { foreignKey: "DISH_id" })

module.exports = function(api){

	api.post('/user', function(req, res, next) {
		var email = req.body.email
		var pseudo = req.body.pseudo
		var password = req.body.password
		var image = req.body.IMAGE_id

		if (image == null)
			image = 1

		Person.find({
			where: {
				email: email
			}
		}).then(function(userEmail) {
			if (userEmail) {
				return res.status(409).send("Cette addresse email est déjà utilisée pour un autre compte")
			} else {
				Person.find({
					where: {
						pseudo: pseudo
					}
				}).then(function(userPseudo) {
					if (userPseudo) {
						return res.status(409).send("Ce pseudo est déjà utilisé")
					} else {
						Image.find({
							where : {
								idIMAGE: image
							}
						}).then(function(image) {
							if (!image) {
								return res.status(404).send("Aucune image n'existe pour cet ID")
							} else {
								Person.create({ email: email, pseudo: pseudo, password: password, isAdmin: false, IMAGE_id: image.idIMAGE }).then(function(user) {
									if (user) {
										return res.status(200).send(user)
									} else {
										return res.status(503).send("Erreur")
									}
								})
							}
						})
					}
				})
			}
		})
	})

	api.post('/user/favorite/:pseudo/:favoris', function(req, res, next) {
		var pseudo = req.params.pseudo
		var favoris = req.params.favoris

		if (!pseudo)
			return res.status(400).send("Pseudo utilisateur manquant")

		if (!favoris)
			return res.status(400).send("Pseudo favoris manquant")

		Person.find({
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Person.find({
					where: {
						pseudo: favoris
					}
				}).then(function(favoris) {
					if (favoris) {

						PersonHasFavorite.find({
							where: {
								idPERSONLike: user.idPERSON,
								idPERSONFavorite: favoris.idPERSON
							}
						}).then(function(personHasFavorite) {
							if (personHasFavorite)
								return res.status(409).send("Utilisateur déjà dans les favoris")
							else {
								PersonHasFavorite.create({ idPERSONLike: user.idPERSON, idPERSONFavorite: favoris.idPERSON }).then(function(favorite) {
									if (favorite) {
										favorite = favorite.toJSON()
										favorite["message"] = "Favoris ajouté"

										res.type("application/json")
										return res.status(200).send(favorite)
									} else
										return res.status(503).send("Erreur lors de l'ajout du favoris")
								})
							}
						})


					} else
						return res.status(404).send("Aucun utilisateur trouvé avec le pseudo du favoris")
				})
			} else
				return res.status(404).send("Aucun utilisateur trouvé avec le pseudo")
		})
	})

	api.post('/user/rating', function(req, res, next) {
		var rater = req.body.rater
		var to = req.body.to
		var note = req.body.note

		if (!rater)
			return res.status(400).send("Pseudo du votant manquant")

		if (!to)
			return res.status(400).send("Pseudo du voté manquant")

		if (!note)
			return res.status(400).send("Note manquante")

		Person.find({
			where: {
				pseudo: rater
			}
		}).then(function(userRater) {
			if (userRater) {
				Person.find({
					where: {
						pseudo: to
					}
				}).then(function(userTo) {
					if (userTo) {
						PersonHasNote.create({ idPERSONVotant: userRater.idPERSON, idPERSONVotee: userTo.idPERSON, note: note }).then(function(note) {
							if (note) {
								note = note.toJSON()
								note["message"] = "Note ajoutée"

								res.type("application/json")
								return res.status(200).send(note)
							} else {
								return res.status(503).send("Erreur lors de la création de la note")
							}
						})
					} else {
						return res.status(404).send("Aucun utilisateur trouvé avec le pseudo du voté")
					}
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé avec le pseudo du votant")
			}
		})
	})

	api.post('/image', function(req, res, next) {
		var name = req.body.name
		var link = req.body.link
		var isPrincipalImage = req.body.isPrincipalImage
		var recipe = req.body.RECIPE_id
		var dish = req.body.DISH_id

		if (!name)
			return res.status(400).send("Nom manquant")

		if (!link)
			return res.status(400).send("Lien manquant")

		if (!isPrincipalImage)
			return res.status(400).send("Statut manquant")

		if (!recipe)
			recipe = null

		if (!dish)
			dish = null

		if (recipe == null && dish == null) {
			Image.create({ name: name, link: link, isPrincipalImage: isPrincipalImage, RECIPE_id: null, DISH_id: null }).then(function(image) {
				if (image) {
					return res.status(200).send(image)
				} else {
					return res.status(503).send("Erreur lors de la création de l'image")
				}
			})
		} else if (recipe != null) {
			Recipe.find({
				where: {
					idRECIPE: recipe
				}
			}).then(function(completeRecipe) {
				if (completeRecipe) {
					Image.create({ name: name, link: link, isPrincipalImage: isPrincipalImage, RECIPE_id: completeRecipe.idRECIPE, DISH_id: null }).then(function(image) {
						if (image) {
							return res.status(200).send(image)
						} else {
							return res.status(503).send("Erreur lors de la création de l'image")
						}
					})
				} else {
					return res.status(404).send("Aucune recette trouvée pour cet ID")
				}
			})
		} else if (dish != null) {
			Dish.find({
				where: {
					idDISH: dish
				}
			}).then(function(completeDish) {
				if (completeDish) {
					Image.create({ name: name, link: link, isPrincipalImage: isPrincipalImage, RECIPE_id: null, DISH_id: completeDish.idDISH }).then(function(image) {
						if (image) {
							return res.status(200).send(image)
						} else {
							return res.status(503).send("Erreur lors de la création de l'image")
						}
					})
				} else {
					return res.status(404).send("Aucun plat trouvée pour cet ID")
				}
			})
		}
	})

	api.post('/recipe',function(req,res,next){
		var name = req.body.name
		var preparation = req.body.preparation
		var baking = req.body.baking
		var serving = req.body.serving
		var directions = req.body.directions
		var pseudo = req.body.pseudo
		var ingredients = req.body.ingredients
		var isAccepted = false

		if (!ingredients)
			return res.status(400).send("Ingredients manquants")

		if (!name)
			return res.status(400).send("Nom manquant")

		if (!preparation)
			return res.status(400).send("Temps de preparation manquant")

		if (!baking)
			return res.status(400).send("Temps de cuisson manquant")

		if (!serving)
			return res.status(400).send("Nombre de personnes manquant")

		if (!directions)
			return res.status(400).send("Instructions manquantes")

		if(!pseudo)
			return res.status(400).send("Utilisateur manquant")

		Person.find({
			where:{
				pseudo: pseudo
			}
		}).then(function(user){
			if(user){
				Recipe.create({
					name: name,
					preparation: preparation,
					baking: baking,
					serving: serving,
					directions: directions,
					isAccepted: isAccepted,
					PERSON_id: user.idPERSON
				}).then(function(recipe){
					if(recipe){
						var asyncCallsRemaining = ingredients.length
						var key
						var tableauIngredient = new Array()
						var i = 0

						for (key in ingredients) {
							Ingredient.create({
								name: ingredients[key],
								RECIPE_id: recipe.idRECIPE
							}).then(function(ing){
								if(ing){
									if (asyncCallsRemaining == 1) {
										return res.status(200).send(recipe)
									}
									asyncCallsRemaining--;
								}
							})
						}
					} else{
						return res.status(503).send("Impossible de creer un plat")
					}
				})
			} else {
				return res.status(404).send("User inconnu")
			}
		})
	})

	api.post('/counterparties', function(req, res, next) {
		var counterparties = req.body.counterparties

		if (!counterparties || counterparties.length <= 0)
			return res.status(400).send("Contreparties manquantes")

		asyncCallsRemainingForCounterparties = counterparties.length
		counterparties.forEach(function(counterparty) {
			Counterparty.find({
				attributes: ['idCOUNTERPARTY'],
				where: {
					name: counterparty.name,
					priceIfPecuniary: counterparty.priceIfPecuniary
				}
			}).then(function(findedCounterparty) {
				if (findedCounterparty) {
					DishHasCounterparty.create({
						DISH_id: counterparty.DISH_id,
						COUNTERPARTY_id: findedCounterparty.idCOUNTERPARTY
					}).then(function(newCounterparty) {

						if (asyncCallsRemainingForCounterparties == 1) {
							return res.status(200).send("Contreparties crées et ajoutées")
						}

						asyncCallsRemainingForCounterparties--
					})
				} else {
					Counterparty.create({
						name: counterparty.name,
						priceIfPecuniary: counterparty.priceIfPecuniary,
						isFavoriteCounterParty: counterparty.isFavoriteCounterParty,
						TYPECOUNTERPARTY_id: counterparty.TYPECOUNTERPARTY_id
					}).then(function(createCounterparty) {
						DishHasCounterparty.create({
							DISH_id: counterparty.DISH_id,
							COUNTERPARTY_id: createCounterparty.idCOUNTERPARTY
						}).then(function(newCounterparty) {

							if (asyncCallsRemainingForCounterparties == 1) {
								return res.status(200).send("Contreparties crées et ajoutées")
							}

							asyncCallsRemainingForCounterparties--
						})
					})
				}
			})
		})
	})

	api.post('/times', function(req, res, next) {
		var times = req.body.times

		if (!times || times.length <= 0)
			return res.status(400).send("Temps manquants")

		asyncCallsRemainingForTimes = times.length
		times.forEach(function(time) {
			var hour = parseInt(time.time.substring(0, 2)) + 2
			var minutes = parseInt(time.time.substring(3, 5))
			if (minutes == 0)
				completeTime = time.date + ' ' + hour + ":00:00"
			else
				completeTime = time.date + ' ' + hour + ":" + minutes + ":00"

			Time.create({
				date: time.date,
				time: completeTime,
				isDepositDate: time.isDepositeDate,
				isEndDate: time.isEndDate,
				DISH_id: time.DISH_id
			}).then(function(createdTime) {

				if (asyncCallsRemainingForTimes == 1) {
					return res.status(200).send("Dates correctement ajoutées")
				}

				asyncCallsRemainingForTimes--
			})
		})
	})

	api.post('/dish',function(req, res, next){
		var name = req.body.name
		var dishDescription = req.body.dishDescription
		var advices= req.body.advices
		var pseudo = req.body.pseudo

		var ADDRESS_id = req.body.ADDRESS_id

		var firstName = req.body.firstName
		var lastName = req.body.lastName
		var address = req.body.address
		var addressComplement1 = req.body.addressComplement1
		var addressComplement2 = req.body.addressComplement2
		var zipCode = req.body.zipCode
		var city = req.body.city

		if (!name)
			return res.status(400).send("Nom manquant")

		if (!dishDescription)
			return res.status(400).send("Description manquante")

		if (!advices)
			return res.status(400).send("Conseils manquants")

		if(!pseudo)
			return res.status(400).send("User manquant")

		if (!ADDRESS_id) {
			if (!firstName && !lastName && !address && !zipCode && !city) {
				return res.status(400).send("Adresse manquante")
			}
		}


		Person.find({
			attributes: ['idPERSON'],
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Dish.create({
					name: name,
					dishDescription: dishDescription,
					advice: advices,
					isReserved: false,
					isInProgress: false,
					Creator_id: user.idPERSON,
					Customer_id: null
				}).then(function(dish){
					if(dish) {
						dish = dish.toJSON()
						Address.find({
							where: {
								idADDRESS: ADDRESS_id
							}
						}).then(function(findedAddress) {
							if (findedAddress) {
								findedAddress.update({ DISH_id: dish.idDISH }, {fields: ['DISH_id']}).then(function(completeDish) {
									if (completeDish) {
										dish["address"] = findedAddress
										return res.status(200).send(dish)
									} else {
										return res.status(503).send("Impossible d'ajouter l'adresse au plat.")
									}
								})
							} else {
								Address.find({
									where: {
										firstName: firstName,
										lastName: lastName,
										address: address,
										addressComplement1: addressComplement1,
										addressComplement2: addressComplement2,
										zipCode: zipCode,
										city: city
									}
								}).then(function(findedAddress2) {
									if (findedAddress2) {
										findedAddress2.update({ DISH_id: dish.idDISH }, {fields: ['DISH_id']}).then(function(completeDish) {
											if (completeDish) {
												dish["address"] = findedAddress2

												PersonHasAddress.create({
													ADDRESS_id: findedAddress2.idADDRESS,
													PERSON_id: user.idPERSON
												}).then(function(personHasAddress) {
													if (personHasAddress) {
														res.type("application/json")
														return res.status(200).send(dish)
													} else {
														return res.status(503).send("Une erreur est survenue.")
													}
												})
											} else {
												return res.status(503).send("Impossible d'ajouter l'adresse au plat.")
											}
										})
									} else {
										Address.create({
											firstName: firstName,
											lastName: lastName,
											address: address,
											addressComplement1: addressComplement1,
											addressComplement2: addressComplement2,
											zipCode: zipCode,
											city: city,
											DISH_id: dish.idDISH
										}).then(function(createdAddress) {
											dish["address"] = createdAddress

											PersonHasAddress.create({
												ADDRESS_id: createdAddress.idADDRESS,
												PERSON_id: user.idPERSON
											}).then(function(personHasAddress) {
												if (personHasAddress) {
													res.type("application/json")
													return res.status(200).send(dish)
												} else {
													return res.status(503).send("Une erreur est survenue.")
												}
											})
										})
									}
								})
							}
						})
					} else {
						return res.status(404).send("Impossible de créer le plat")
					}
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé.")
			}
		})
	})
}
