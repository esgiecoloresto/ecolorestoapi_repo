var mysql = require('mysql')
var bodyparser = require('body-parser')
var Sequelize = require('sequelize')
var ENDPOINT = require('../ENDPOINT.js')

var sequelize = new Sequelize(ENDPOINT.endpoint, {
	define: {
		timestamps: false
	},
	logging: false
});

/* Déclaration Models */
var Address = sequelize.import("../models/address")
var Counterparty = sequelize.import("../models/counterparty")
var DishHasCounterparty = sequelize.import("../models/dish_has_counterparty")
var Dish = sequelize.import("../models/dish")
var Image = sequelize.import("../models/image")
var Ingredient = sequelize.import("../models/ingredient")
var PersonHasAddress = sequelize.import("../models/person_has_address")
var PersonHasNote = sequelize.import("../models/person_has_note")
var PersonHasFavorite = sequelize.import("../models/person_has_favorite")
var Person = sequelize.import("../models/person")
var Recipe = sequelize.import("../models/recipe")
var Time = sequelize.import("../models/time")
var TypeCounterparty = sequelize.import("../models/typecounterparty")

/* Declaration relations */
Address.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasOne(Address, { foreignKey: "DISH_id" })
Counterparty.belongsTo(TypeCounterparty, { foreignKey: "TYPECOUNTERPARTY_id" })
TypeCounterparty.hasMany(Counterparty, { foreignKey: "TYPECOUNTERPARTY_id" })

Dish.belongsTo(Person, { as: "auteur", foreignKey: "Creator_id" })
Person.hasMany(Dish, { as: "auteur", foreignKey: "Creator_id" })
Dish.belongsTo(Person, { as: "client", foreignKey: "Customer_id" })
Person.hasMany(Dish, { as: "client", foreignKey: "Customer_id" })

Dish.belongsToMany(Counterparty, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "DISH_id" })
Counterparty.belongsToMany(Dish, { through: {model: DishHasCounterparty, unique: false}, foreignKey: "COUNTERPARTY_id" })

Image.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Image, { foreignKey: "DISH_id" })
Image.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Image, { foreignKey: "RECIPE_id" })

Ingredient.belongsTo(Recipe, { foreignKey: "RECIPE_id" })
Recipe.hasMany(Ingredient, { foreignKey: "RECIPE_id" })

Person.belongsTo(Image, { foreignKey: "IMAGE_id" })
Image.hasOne(Person, { foreignKey: "IMAGE_id" })

Person.belongsToMany(Person, { as: "idPERSONVotant", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotant" })
Person.belongsToMany(Person, { as: "idPERSONVotee", through: {model: PersonHasNote, unique: false}, foreignKey: "idPERSONVotee" })

Person.belongsToMany(Address, { through: {model: PersonHasAddress, unique: false}, foreignKey: "PERSON_id" })
Address.belongsToMany(Person, { through: {model: PersonHasAddress, unique: false}, foreignKey: "ADDRESS_id" })

Person.belongsToMany(Person, { as: "favorites", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONLike" })
Person.belongsToMany(Person, { as: "likedBy", through: {model: PersonHasFavorite, unique: false}, foreignKey: "idPERSONFavorite" })

Recipe.belongsTo(Person, { foreignKey: "PERSON_id" })
Person.hasMany(Recipe, { foreignKey: "PERSON_id" })
Time.belongsTo(Dish, { foreignKey: "DISH_id" })
Dish.hasMany(Time, { foreignKey: "DISH_id" })

module.exports = function(api){

	/*api.delete('/recipes/:name', function(req, res, next) {

		var name = req.params.name
			Recipe.destroy({
				where: {
					name: name
				}
			}).then(function(recipe) {
					if(recipe){
						return res.status(200).send("delete accomplised");
					}
					else {
						return res.status(404).send("error recipe not found");
					}
			})
		})*/

	api.delete('/recipes/:name/:pseudo', function(req, res, next) {
		var name = req.params.name
		var pseudo = req.params.pseudo

		Person.find({
			attributes: ['idPERSON'],
			where: {
				pseudo: pseudo
			}
		}).then(function(user) {
			if (user) {
				Recipe.find({
					attributes: ['idRECIPE'],
					where: {
						name: name,
						PERSON_id: user.idPERSON
					}
				}).then(function(recipe) {
					if (recipe) {
						Image.destroy({
							where: {
								RECIPE_id: recipe.idRECIPE
							}
						}).then(function(affectedRows) {

							Ingredient.destroy({
								where: {
									RECIPE_id: recipe.idRECIPE
								}
							}).then(function(affectedRowsIngredients) {
								Recipe.destroy({
									where: {
										idRECIPE: recipe.idRECIPE
									}
								}).then(function(affectedRowsRecipe) {
									if (affectedRowsRecipe == 1) {
										return res.status(200).send("Une recette a été supprimée")
									} else if (affectedRowsRecipe > 1) {
										return res.status(200).send("Plusieurs recettes ont été supprimées")
									} else {
										return res.status(503).send("Aucune recette n'a été supprimée")
									}
								})
							})
						})
					} else {
						return res.status(404).send("Aucune recette trouvée")
					}
				})
			} else {
				return res.status(404).send("Aucun utilisateur trouvé")
			}
		})
	})


	api.delete('/user/:pseudo', function(req, res, next) {

		var pseudo = req.params.pseudo
		Person.destroy({
			where: {
				pseudo: pseudo
			}
		}).then(function(affectedRows) {
			if(affectedRows > 0){
				return res.status(200).send("Utilisateur supprimé");
			} else {
				return res.status(404).send("Utilisateur non trouvé");
			}
		})
	})
}
