/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person_has_favorite', {
    idPERSONLike: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    idPERSONFavorite: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'person_has_favorite'
  });
};
