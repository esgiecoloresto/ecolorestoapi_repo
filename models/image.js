/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('image', {
    idIMAGE: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    link: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isPrincipalImage: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    RECIPE_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'recipe',
        key: 'idRECIPE'
      }
    },
    DISH_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'image'
  });
};
