/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person_has_note', {
    idPERSON_has_NOTE: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    idPERSONVotant: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    idPERSONVotee: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    note: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'person_has_note'
  });
};
