/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dish_has_counterparty', {
    DISH_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    COUNTERPARTY_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'dish_has_counterparty'
  });
};
