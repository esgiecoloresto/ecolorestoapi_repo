/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('counterparty', {
    idCOUNTERPARTY: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    priceIfPecuniary: {
      type: 'DOUBLE',
      allowNull: true
    },
    isFavoriteCounterParty: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    TYPECOUNTERPARTY_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'counterparty'
  });
};
