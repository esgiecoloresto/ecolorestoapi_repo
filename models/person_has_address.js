/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person_has_address', {
    ADDRESS_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    PERSON_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'person_has_address'
  });
};
