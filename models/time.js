/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('time', {
    idTIME: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    isDepositDate: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    isEndDate: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    DISH_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'time'
  });
};
