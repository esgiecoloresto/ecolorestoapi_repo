/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dish', {
    idDISH: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    dishDescription: {
      type: DataTypes.STRING,
      allowNull: false
    },
    advice: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isReserved: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    isInProgress: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    Creator_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Customer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'dish'
  });
};
