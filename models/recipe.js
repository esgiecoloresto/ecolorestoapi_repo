/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('recipe', {
    idRECIPE: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    preparation: {
      type: DataTypes.STRING,
      allowNull: false
    },
    baking: {
      type: DataTypes.STRING,
      allowNull: false
    },
    serving: {
      type: DataTypes.STRING,
      allowNull: false
    },
    directions: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    isAccepted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    PERSON_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'recipe'
  });
};
