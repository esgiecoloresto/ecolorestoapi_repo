SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecoloresto`
--
CREATE DATABASE IF NOT EXISTS `ecoloresto` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecoloresto`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `idADDRESS` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  `addressComplement1` varchar(255),
  `addressComplement2` varchar(255),
  `zipCode` varchar(5) NOT NULL,
  `city` varchar(45) NOT NULL,
  `DISH_id` int(11) NOT NULL UNIQUE,
  PRIMARY KEY (`idADDRESS`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `counterparty`
--

CREATE TABLE IF NOT EXISTS `counterparty` (
  `idCOUNTERPARTY` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `priceIfPecuniary` double,
  `isFavoriteCounterParty` tinyint(1) NOT NULL,
  `TYPECOUNTERPARTY_id` int(11) NOT NULL,
  PRIMARY KEY (`idCOUNTERPARTY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE IF NOT EXISTS `dish` (
  `idDISH` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dishDescription` varchar(500) NOT NULL,
  `advice` varchar(500) NOT NULL,
  `isReserved` tinyint(1) NOT NULL,
  `isInProgress` tinyint(1) NOT NULL,
  `Creator_id` int(11) NOT NULL,
  `Customer_id` int(11),
  PRIMARY KEY (`idDISH`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dish_has_counterparty`
--

CREATE TABLE IF NOT EXISTS `dish_has_counterparty` (
  `DISH_id` int(11) NOT NULL,
  `COUNTERPARTY_id` int(11) NOT NULL,
  PRIMARY KEY (`COUNTERPARTY_id`, `DISH_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `idIMAGE` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `link` varchar(500) NOT NULL,
  `isPrincipalImage` tinyint(1) NOT NULL,
  `RECIPE_id` int(11),
  `DISH_id` int(11),
  PRIMARY KEY (`idIMAGE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE IF NOT EXISTS `ingredient` (
  `idINGREDIENT` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `RECIPE_id` int(11) NOT NULL,
  PRIMARY KEY (`idINGREDIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `idPERSON` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(45) NOT NULL UNIQUE,
  `email` varchar(255) NOT NULL UNIQUE,
  `password` varchar(64) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `IMAGE_id` int(11),
  PRIMARY KEY (`idPERSON`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_has_note`
--

CREATE TABLE IF NOT EXISTS `person_has_note` (
  `idPERSON_has_NOTE` int(11) NOT NULL AUTO_INCREMENT,
  `idPERSONVotant` int NOT NULL,
  `idPERSONVotee` int NOT NULL,
  `note` int NOT NULL,
  PRIMARY KEY (`idPERSON_has_NOTE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `person_has_address`
--

CREATE TABLE IF NOT EXISTS `person_has_address` (
  `ADDRESS_id` int(11) NOT NULL,
  `PERSON_id` int(11) NOT NULL,
  PRIMARY KEY (`ADDRESS_id`, `PERSON_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `person_has_favorite`
--

CREATE TABLE IF NOT EXISTS `person_has_favorite` (
  `idPERSONLike` int(11) NOT NULL,
  `idPERSONFavorite` int(11) NOT NULL,
  PRIMARY KEY (`idPERSONLike`, `idPERSONFavorite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `idRECIPE` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `preparation` varchar(45) NOT NULL,
  `baking` varchar(45) NOT NULL,
  `serving` varchar(45) NOT NULL,
  `directions` text NOT NULL,
  `isAccepted` tinyint(1) NOT NULL,
  `PERSON_id` int(11) NOT NULL,
  PRIMARY KEY (`idRECIPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE IF NOT EXISTS `time` (
  `idTIME` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `isDepositDate` tinyint(1) NOT NULL,
  `isEndDate` tinyint(1) NOT NULL,
  `DISH_id` int(11) NOT NULL,
  PRIMARY KEY (`idTIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `typecounterparty`
--

CREATE TABLE IF NOT EXISTS `typecounterparty` (
  `idTYPECOUNTERPARTY` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idTYPECOUNTERPARTY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typecounterparty`
--

INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(1, "Pécunier");
INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(2, "Bien Matériel / Objet");
INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(3, "Service");


ALTER TABLE address ADD CONSTRAINT fk_address_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
ALTER TABLE counterparty ADD CONSTRAINT fk_counterparty_type FOREIGN KEY (TYPECOUNTERPARTY_id) REFERENCES typecounterparty(idTYPECOUNTERPARTY);

ALTER TABLE dish ADD CONSTRAINT fk_dish_creator FOREIGN KEY (Creator_id) REFERENCES person(idPERSON);
ALTER TABLE dish ADD CONSTRAINT fk_dish_customer FOREIGN KEY (Customer_id) REFERENCES person(idPERSON);

ALTER TABLE dish_has_counterparty ADD CONSTRAINT fk_dish_counterparty FOREIGN KEY (COUNTERPARTY_id) REFERENCES counterparty(idCOUNTERPARTY);
ALTER TABLE dish_has_counterparty ADD CONSTRAINT fk_dish_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);

ALTER TABLE image ADD CONSTRAINT fk_image_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
ALTER TABLE image ADD CONSTRAINT fk_image_recipe FOREIGN KEY (RECIPE_id) REFERENCES recipe(idRECIPE);

ALTER TABLE ingredient ADD CONSTRAINT fk_ingredient_recipe FOREIGN KEY (RECIPE_id) REFERENCES recipe(idRECIPE);

ALTER TABLE person ADD CONSTRAINT fk_person_image FOREIGN KEY (IMAGE_id) REFERENCES image(idIMAGE);

ALTER TABLE person_has_note ADD CONSTRAINT fk_person_note_votant FOREIGN KEY (idPERSONVotant) REFERENCES person(idPERSON);
ALTER TABLE person_has_note ADD CONSTRAINT fk_person_note_votee FOREIGN KEY (idPERSONVotee) REFERENCES person(idPERSON);

ALTER TABLE person_has_address ADD CONSTRAINT fk_person_address FOREIGN KEY (ADDRESS_id) REFERENCES address(idADDRESS);
ALTER TABLE person_has_address ADD CONSTRAINT fk_person_person FOREIGN KEY (PERSON_id) REFERENCES person(idPERSON);

ALTER TABLE person_has_favorite ADD CONSTRAINT fk_person_person_like FOREIGN KEY (idPERSONLike) REFERENCES person(idPERSON);
ALTER TABLE person_has_favorite ADD CONSTRAINT fk_person_person_favorite FOREIGN KEY (idPERSONFavorite) REFERENCES person(idPERSON);

ALTER TABLE recipe ADD CONSTRAINT fk_recipe_person FOREIGN KEY (PERSON_id) REFERENCES person(idPERSON);
ALTER TABLE time ADD CONSTRAINT fk_time_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
