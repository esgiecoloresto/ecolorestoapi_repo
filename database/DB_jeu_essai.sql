SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecoloresto`
--
CREATE DATABASE IF NOT EXISTS `ecoloresto` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecoloresto`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `idADDRESS` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  `addressComplement1` varchar(255),
  `addressComplement2` varchar(255),
  `zipCode` varchar(5) NOT NULL,
  `city` varchar(45) NOT NULL,
  `DISH_id` int(11) NOT NULL UNIQUE,
  PRIMARY KEY (`idADDRESS`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(1, 'Pierre', 'BOUDON', '4000 Place de la Nation', NULL, NULL, '75012', 'Paris', 1);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(2, 'Nicolas', 'KERVOERN', '30 Rue Claude Tillier', NULL, NULL, '75012', 'Paris', 2);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(3, 'Pierre', 'BOUDON', '107 Boulevard Diderot', NULL, NULL, '75012', 'Paris', 3);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(4, 'Pierre', 'BOUDON', '22 Passage du Génie', NULL, NULL, '75012', 'Paris', 4);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(5, 'Nicolas', 'KERVOERN', '1 Rue Picpus', NULL, NULL, '75012', 'Paris', 5);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(6, 'Nicolas', 'GIDON', '220 Rue du Faubourg Saint-Antoine', NULL, NULL, '75012', 'Paris', 6);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(7, 'Nicolas', 'KERVOERN', '35 Rue de Reuilly', NULL, NULL, '75012', 'Paris', 7);
INSERT INTO `address` (`idADDRESS`, `firstName`, `lastName`, `address`, `addressComplement1`, `addressComplement2`, `zipCode`, `city`, `DISH_id`) VALUES
(8, 'Nicolas', 'GIDON', '8 Cour Saint-Eloi', NULL, NULL, '75012', 'Paris', 8);


-- --------------------------------------------------------

--
-- Table structure for table `counterparty`
--

CREATE TABLE IF NOT EXISTS `counterparty` (
  `idCOUNTERPARTY` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `priceIfPecuniary` double,
  `isFavoriteCounterParty` tinyint(1) NOT NULL,
  `TYPECOUNTERPARTY_id` int(11) NOT NULL,
  PRIMARY KEY (`idCOUNTERPARTY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counterparty`
--

INSERT INTO `counterparty` (`idCOUNTERPARTY`, `name`, `priceIfPecuniary`, `isFavoriteCounterParty`, `TYPECOUNTERPARTY_id`) VALUES
(1, 'Echarpe', NULL, 0, 2);
INSERT INTO `counterparty` (`idCOUNTERPARTY`, `name`, `priceIfPecuniary`, `isFavoriteCounterParty`, `TYPECOUNTERPARTY_id`) VALUES
(2, 'Règlement en espèces', 10.0, 1, 1);
INSERT INTO `counterparty` (`idCOUNTERPARTY`, `name`, `priceIfPecuniary`, `isFavoriteCounterParty`, `TYPECOUNTERPARTY_id`) VALUES
(3, 'Tableau', NULL, 0, 2);
INSERT INTO `counterparty` (`idCOUNTERPARTY`, `name`, `priceIfPecuniary`, `isFavoriteCounterParty`, `TYPECOUNTERPARTY_id`) VALUES
(4, 'Aide aux devoirs pour enfants', NULL, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE IF NOT EXISTS `dish` (
  `idDISH` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dishDescription` varchar(500) NOT NULL,
  `advice` varchar(500) NOT NULL,
  `isReserved` tinyint(1) NOT NULL,
  `isInProgress` tinyint(1) NOT NULL,
  `Creator_id` int(11) NOT NULL,
  `Customer_id` int(11),
  PRIMARY KEY (`idDISH`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(1, 'Burger', "Je propose des Burgers car j'en ai trop fait et que les ingrédients doivent être consommés rapidement. Ces Burgers sont très bons et sont composés de Fromage, de Viande, de Bacon, de Tomates et de Salade. Sans sauce vous pourrez les personnaliser et les manger avec votre sauce préférée.", 'Je conseille de réchauffer quelques minutes au four avant consommation pour que les pains soient moelleux et que le Burger et la Viande soient chaud. Pensez aussi à mettre votre sauce.', 0, 0, 1, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(2, 'Rôti de Porc', "Bonjour.\nAyant des invités j'ai fait un gros Rôti de Porc. Cependant il est beaucoup trop gros pour être consommé entièrement ce soir.\nAinsi je propose 4 Tranches de Rôti de Porc. Pouvant être accompagnés de quelques légumes (haricots verts), vous pourrez les manger ou les conserver au congélateur.", 'Aucun conseil spécial concernant ce plat, juste déguster avec un bon vin.', 0, 0, 2, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(3, 'Tartiflette', 'Tartiflette faite maison avec les produits du jardin. Produits en grande quantité donc plat en grosse quantité. Ainsi je met à disposition des parts (3 max) de cette Tartiflette maison.', 'Eviter de réchauffer au Micro-Onde. Une 10aine de minutes au four suffira pour pouvoir déguster cette Tartiflette chaude. Vous pouvez garder ce plats quelques jours au frais avant de le consommer (maximum 3 jours)', 0, 0, 1, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(4, 'Kouign-Amann', 'Spécialité Bretonne, ce délicieux Kouign-Amann vous permettra de déguster ce plat typique de Bretagne. Plat riche en sucre, en calories et surtout en goût, vous pourrez le déguster en entier. En effet je propose un Kouign-Amann en entier', 'Préchauffez votre four avant votre repas et déposé le Kouign-Amann dans le four chaud éteint pendant la durée de votre repas. Ainsi vous pourrez déguster un Kouign-Amann entre chaud et tiède : la température parfaite pour ce plat.', 0, 0, 1, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(5, 'Gâteau au Chocolat', "J'ai fait 2 Gâteaux au chocolat pour l'anniversaire de ma fille. N'en ayant consommé qu'un seul je propose l'autre, aujourd'hui.", 'Pas de conseils, dégustez ce gâteau comme vous le souhaitez.', 0, 0, 2, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(6, 'Gratin Dauphinois', "3 cassolettes de Gratin Dauphinois sont disponibles. J'en ai fait beaucoup plus que ce qu'il fallait c'est pour cela que je les propose.", 'Vous pouvez réchauffer ces cassolettes au Micro-Onde quelques minutes pour les déguster chaudes', 0, 0, 3, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(7, 'Tournedos Rossini', "J'avais préparé 4 Tournedos Rossini mais mon amie et moi n'avons pas apprécié. Je propose ainsi 2 parts de Tournedos avec ses ingrédients supplémentaires (sauce, foie gras)", 'Viande donnée cure pour que vous puissiez faire votre propre cuisson. A cuire à la poêle dans une noix de beurre. Saisir à feu vif avant de retirer du feu.', 0, 0, 2, NULL);
INSERT INTO `dish` (`idDISH`, `name`, `dishDescription`, `advice`, `isReserved`, `isInProgress`, `Creator_id`, `Customer_id`) VALUES
(8, 'Lasagnes', "J'ai fait 2 plats de Lasagnes pour manger avec ma famille mais on en a mangé qu'un seul. Je propose donc un plat complet de Lasagnes. Ces Lasagnes sont faites maison.", 'Lasagnes directement consommable après réception et rechauffage du plat soit congelable pour une degustation plus tard. Eviter de garder plus de 3 jours.', 0, 0, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dish_has_counterparty`
--

CREATE TABLE IF NOT EXISTS `dish_has_counterparty` (
  `DISH_id` int(11) NOT NULL,
  `COUNTERPARTY_id` int(11) NOT NULL,
  PRIMARY KEY (`COUNTERPARTY_id`, `DISH_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dish_has_counterparty`
--

INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(1, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(1, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(1, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(1, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(2, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(2, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(2, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(2, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(3, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(3, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(3, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(3, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(4, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(4, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(4, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(4, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(5, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(5, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(5, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(5, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(6, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(6, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(6, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(6, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(7, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(7, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(7, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(7, 4);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(8, 1);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(8, 2);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(8, 3);
INSERT INTO `dish_has_counterparty` (`DISH_id`, `COUNTERPARTY_id`) VALUES
(8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `idIMAGE` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `link` varchar(500) NOT NULL,
  `isPrincipalImage` tinyint(1) NOT NULL,
  `RECIPE_id` int(11),
  `DISH_id` int(11),
  PRIMARY KEY (`idIMAGE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(1, "ImageByDefault", "https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg", 1, NULL, NULL);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(2, "Burger1", "http://www.blog.marche-prive.com/wp-content/uploads/2015/04/food58-burger-two-bleu-bacon.jpg", 1, NULL, 1);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(3, "Burger2", "http://img1.gtsstatic.com/wallpapers/9b04890a3b057f005949ee85b16fc931_large.jpeg", 0, NULL, 1);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(4, "Roti1", "http://cdn-elle.ladmedia.fr/var/plain_site/storage/images/elle-a-table/recettes-de-cuisine/carre-de-porc-au-chorizo/7622-1-fre-FR/carre-de-porc-au-chorizo_visuel_recette.jpg", 1, NULL, 2);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(5, "Roti2", "http://s7d9.scene7.com/is/image/SAQ/roti-de-porc-moutarde-gingembre?$saq-recet$", 0, NULL, 2);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(6, "Roti3", "http://www.coupdepouce.com/img/photos/biz/CoupDePouce/recettes_non_importees/Plats%20principaux/roti-porc-moutarde410.jpg", 0, NULL, 2);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(7, "Tartiflette1", "http://icu.linter.fr/750/10002060/1496690307/tartiflette.jpg", 1, NULL, 3);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(8, "Tartiflette2", "http://static.750g.com/images/487-277/e2cf6b454dbab8fde0a57d8fee8025ed/tartiflette.jpeg", 0, NULL, 3);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(9, "Kouign-Amann1", "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Kouignamann.JPG/280px-Kouignamann.JPG", 1, NULL, 4);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(10, "Kouign-Amann1", "http://www.foodreporter.fr/upload/original/3/z/o/j/a/347670.jpg", 0, NULL, 4);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(11, "GateauChocoloat1", "http://static.750g.com/images/200-160/544275ada98c1badf4e8848fea6ff658/gateau-au-chocolat-3-etoiles.png", 1, NULL, 5);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(12, "Gratin dauphinois1", "http://www.cuisineactuelle.fr/var/cui/storage/images/recettes-de-cuisine/recettes-pour-tous/familiale/gratin-dauphinois-facile/1089529-2-fre-FR/gratin-dauphinois-facile.jpg", 1, NULL, 6);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(13, "Gratin dauphinois2", "http://macuisinecreole.com/wp-content/uploads/2015/03/cuisine-antillaise-gratin-dauphinois-1.jpg", 0, NULL, 6);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(14, "Gratin dauphinois3", "http://images.cuisineaz.com/680x357/49d72689-c5cf-4ca6-9b74-95aac93d110e.jpg", 0, NULL, 6);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(15, "Tournedos1", "http://www.delices-du-monde.fr/photos-recettes/zoom/01-tournedos-rossini.jpg", 1, NULL, 7);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(16, "Tournedos2", "http://www.lacuisinedefabrice.fr/wp-content/uploads/2009/03/tournedos-rossini.jpg", 0, NULL, 7);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(17, "Tournedos3", "https://s-media-cache-ak0.pinimg.com/736x/7b/bf/c4/7bbfc4086c9ab8afa7859afbba96b16f.jpg", 0, NULL, 7);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(18, "Lasagnes1", "http://sttmezeriat.fr/wp-content/uploads/2015/09/lasagne-4.jpg", 1, NULL, 8);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(19, "Lasagnes2", "http://boucherie-cheval.fr/photos-viande-cheval/photo-lasagnes-cheval-lasagne-findus-bolognaise.png", 0, NULL, 8);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(20, "ProfilImagePierre", "https://lh4.googleusercontent.com/-yvDZHxTUzZg/AAAAAAAAAAI/AAAAAAAAAAA/AMW9IgfPYUvVK7Q5sjWLob8pxqXyeJMTMQ/s96-c-mo/photo.jpg", 1, NULL, NULL);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(21, "ProfilImageNicoK", "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/431657_4612812116846_378782163_n.jpg?oh=3dbee2d0784fd15c521372f4ddc7701d&oe=57B9A349", 1, NULL, NULL);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(22, "ProfilImageNicoGid", "https://scontent-cdg2-1.xx.fbcdn.net/t31.0-8/s960x960/1053450_627444657266264_878168735_o.jpg", 1, NULL, NULL);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(23, "ProfilImageBetaTesteur", "https://lh4.googleusercontent.com/-yvDZHxTUzZg/AAAAAAAAAAI/AAAAAAAAAA/AMW9IgfPYUvVK7Q5sjWLob8pxqXyeJMTMQ/s96-c-mo/photo.jpg", 1, NULL, NULL);

INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(24, "Burger1", "http://www.blog.marche-prive.com/wp-content/uploads/2015/04/food58-burger-two-bleu-bacon.jpg", 1, 1, NULL);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(25, "Burger2", "http://img1.gtsstatic.com/wallpapers/9b04890a3b057f005949ee85b16fc931_large.jpeg", 0, 1, NULL);
INSERT INTO `image` (`idIMAGE`, `name`, `link`, `isPrincipalImage`, `RECIPE_id`, `DISH_id`) VALUES
(26, "CakeCroqueMonsieur1", "http://auxmilledelices.com/wp-content/uploads/2016/02/Croque-cake-2-e1454777830420.jpg", 1, 2, NULL);


-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE IF NOT EXISTS `ingredient` (
  `idINGREDIENT` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `RECIPE_id` int(11) NOT NULL,
  PRIMARY KEY (`idINGREDIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingredient`
--

INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(1, "Pains Burger", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(2, "Steak haché", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(3, "Tomates", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(4, "Salade", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(5, "Oignons rouges", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(6, "Mayonnaise", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(7, "Bacon", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(8, "Cornichons", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(9, "100g de Fromage", 1);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(10, "12 Tranches de Pain de Mie", 2);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(11, "6 Tranches de Jambon", 2);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(12, "6 Tranches d'Emmental", 2);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(13, "2 Oeufs", 2);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(14, "20cl de crème fraîche", 2);
INSERT INTO `ingredient` (`idINGREDIENT`, `name`, `RECIPE_id`) VALUES
(15, "Sel, Poivre, Muscade", 2);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `idPERSON` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(45) NOT NULL UNIQUE,
  `email` varchar(255) NOT NULL UNIQUE,
  `password` varchar(64) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `IMAGE_id` int(11),
  PRIMARY KEY (`idPERSON`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`idPERSON`, `pseudo`, `email`, `password`, `isAdmin`, `IMAGE_id`) VALUES
(1, 'Pierre', 'pierre@me.com', '25cad6463b5ea76e9341557b408b990741659036c39737867caf8c2965885005', 1, 24); -- PierreMDP
INSERT INTO `person` (`idPERSON`, `pseudo`, `email`, `password`, `isAdmin`, `IMAGE_id`) VALUES
(2, 'NicoK', 'nicok@me.com', 'dc1ba8fb3f76dc768a2171fd2f455840b63c0a89667738a238643e5370895f5a', 1, 25); -- NicoKMDP
INSERT INTO `person` (`idPERSON`, `pseudo`, `email`, `password`, `isAdmin`, `IMAGE_id`) VALUES
(3, 'NicoGid', 'nicogid@me.com', '37329c79764234c405a75b4d02047163bbc0f6cdeb761ad4a38a3953702b52b9', 1, 26); -- NicoGidMDP


-- --------------------------------------------------------

--
-- Table structure for table `person_has_note`
--

CREATE TABLE IF NOT EXISTS `person_has_note` (
  `idPERSON_has_NOTE` int(11) NOT NULL AUTO_INCREMENT,
  `idPERSONVotant` int NOT NULL,
  `idPERSONVotee` int NOT NULL,
  `note` int NOT NULL,
  PRIMARY KEY (`idPERSON_has_NOTE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_has_note`
--

INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(1, 2, 1, 5);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(2, 3, 1, 5);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(3, 2, 1, 4);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(4, 3, 1, 5);

INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(6, 1, 2, 4);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(7, 3, 2, 4);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(8, 1, 2, 5);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(9, 3, 2, 4);

INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(11, 1, 3, 1);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(12, 2, 3, 1);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(13, 1, 3, 1);
INSERT INTO `person_has_note` (`idPERSON_has_NOTE`, `idPERSONVotant`, `idPERSONVotee`, `note`) VALUES
(14, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person_has_address`
--

CREATE TABLE IF NOT EXISTS `person_has_address` (
  `ADDRESS_id` int(11) NOT NULL,
  `PERSON_id` int(11) NOT NULL,
  PRIMARY KEY (`ADDRESS_id`, `PERSON_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_has_address`
--

INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(1, 1);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(2, 2);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(3, 1);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(4, 1);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(5, 2);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(6, 3);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(7, 2);
INSERT INTO `person_has_address` (`ADDRESS_id`, `PERSON_id`) VALUES
(8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `person_has_favorite`
--

CREATE TABLE IF NOT EXISTS `person_has_favorite` (
  `idPERSONLike` int(11) NOT NULL,
  `idPERSONFavorite` int(11) NOT NULL,
  PRIMARY KEY (`idPERSONLike`, `idPERSONFavorite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_has_address`
--

INSERT INTO `person_has_favorite` (`idPERSONLike`, `idPERSONFavorite`) VALUES
(1, 2);
INSERT INTO `person_has_favorite` (`idPERSONLike`, `idPERSONFavorite`) VALUES
(2, 1);
INSERT INTO `person_has_favorite` (`idPERSONLike`, `idPERSONFavorite`) VALUES
(3, 1);
INSERT INTO `person_has_favorite` (`idPERSONLike`, `idPERSONFavorite`) VALUES
(3, 2);


-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `idRECIPE` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `preparation` varchar(45) NOT NULL,
  `baking` varchar(45) NOT NULL,
  `serving` varchar(45) NOT NULL,
  `directions` text NOT NULL,
  `isAccepted` tinyint(1) NOT NULL,
  `PERSON_id` int(11) NOT NULL,
  PRIMARY KEY (`idRECIPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`idRECIPE`, `name`, `preparation`, `baking`, `serving`, `directions`, `isAccepted`, `PERSON_id`) VALUES
(1, "Burger", "20 min", "25 min", "3 pers.", "Cette recette est vraiment très simple.\n1 - Préparez tous les ingrédients ci-dessous sur votre plan de travail.\n2 - Faites chauffer les steaks hachés et le Bacon et disposez les avec les autres ingrédients.\n3 - Assemblez chacun des ingrédients comme bon vous semble sans oublier la sauce.\n4 - C'est prêt ! Vous pouvez déguster !", 0, 1);
INSERT INTO `recipe` (`idRECIPE`, `name`, `preparation`, `baking`, `serving`, `directions`, `isAccepted`, `PERSON_id`) VALUES
(2, "Cake Croque Monsieur", "10 min", "30 - 40 min.", "1 à 2 pers.", "Préchauffez le four à 180°C\nMélangez la crème fraîche, sel, poivre, muscade, oeufs.\nEnlevez la croûte du pain de mie. Disposez dans le fond d'un moule à cake 3 tranches de pain de mie.\nCouvrir de jambon et d'emmental.\nUne nouvelle couche de pain de mie et une couche de votre mélange.\nRecommencez la même opération jusqu'à épuisement des ingrédients.\nEnfournez pendant 30 à 40 minutes selon votre four.", 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE IF NOT EXISTS `time` (
  `idTIME` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `isDepositDate` tinyint(1) NOT NULL,
  `isEndDate` tinyint(1) NOT NULL,
  `DISH_id` int(11) NOT NULL,
  PRIMARY KEY (`idTIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time`
--

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(1, '2016-07-26', '12:00:00', 1, 0, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(2, '2016-07-26', '20:30:00', 0, 1, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(3, '2016-07-26', '18:00:00', 0, 0, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(4, '2016-07-26', '18:30:00', 0, 0, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(5, '2016-07-26', '19:00:00', 0, 0, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(6, '2016-07-26', '19:30:00', 0, 0, 1);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(7, '2016-07-26', '20:00:00', 0, 0, 1);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(8, '2016-07-25', '11:30:00', 1, 0, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(9, '2016-07-25', '20:30:00', 0, 1, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(10, '2016-07-25', '18:00:00', 0, 0, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(11, '2016-07-25', '18:30:00', 0, 0, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(12, '2016-07-25', '19:00:00', 0, 0, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(13, '2016-07-25', '19:30:00', 0, 0, 2);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(14, '2016-07-25', '20:00:00', 0, 0, 2);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(15, '2016-07-24', '11:45:00', 1, 0, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(16, '2016-07-24', '20:30:00', 0, 1, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(17, '2016-07-24', '18:00:00', 0, 0, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(18, '2016-07-24', '18:30:00', 0, 0, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(19, '2016-07-24', '19:00:00', 0, 0, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(20, '2016-07-24', '19:30:00', 0, 0, 3);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(21, '2016-07-24', '20:00:00', 0, 0, 3);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(22, '2016-07-26', '12:00:00', 1, 0, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(23, '2016-07-26', '20:30:00', 0, 1, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(24, '2016-07-26', '18:00:00', 0, 0, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(25, '2016-07-26', '18:30:00', 0, 0, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(26, '2016-07-26', '19:00:00', 0, 0, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(27, '2016-07-26', '19:30:00', 0, 0, 4);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(28, '2016-07-26', '20:00:00', 0, 0, 4);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(29, '2016-07-26', '12:00:00', 1, 0, 5);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(30, '2016-07-26', '20:30:00', 0, 1, 5);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(31, '2016-07-26', '18:00:00', 0, 0, 5);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(32, '2016-07-26', '12:00:00', 1, 0, 6);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(33, '2016-07-26', '20:30:00', 0, 1, 6);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(34, '2016-07-26', '18:00:00', 0, 0, 6);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(35, '2016-07-26', '18:30:00', 0, 0, 6);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(36, '2016-07-26', '12:00:00', 1, 0, 7);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(37, '2016-07-26', '20:30:00', 0, 1, 7);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(38, '2016-07-26', '18:00:00', 0, 0, 7);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(39, '2016-07-26', '18:30:00', 0, 0, 7);

INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(40, '2016-07-26', '12:00:00', 1, 0, 8);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(41, '2016-07-26', '20:30:00', 0, 1, 8);
INSERT INTO `time` (`idTIME`, `date`, `time`, `isDepositDate`, `isEndDate`, `DISH_id`) VALUES
(42, '2016-07-26', '18:00:00', 0, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `typecounterparty`
--

CREATE TABLE IF NOT EXISTS `typecounterparty` (
  `idTYPECOUNTERPARTY` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idTYPECOUNTERPARTY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typecounterparty`
--

INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(1, "Pécunier");
INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(2, "Bien Matériel / Objet");
INSERT INTO `typecounterparty` (`idTYPECOUNTERPARTY`, `name`) VALUES
(3, "Service");


ALTER TABLE address ADD CONSTRAINT fk_address_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
ALTER TABLE counterparty ADD CONSTRAINT fk_counterparty_type FOREIGN KEY (TYPECOUNTERPARTY_id) REFERENCES typecounterparty(idTYPECOUNTERPARTY);

ALTER TABLE dish ADD CONSTRAINT fk_dish_creator FOREIGN KEY (Creator_id) REFERENCES person(idPERSON);
ALTER TABLE dish ADD CONSTRAINT fk_dish_customer FOREIGN KEY (Customer_id) REFERENCES person(idPERSON);

ALTER TABLE dish_has_counterparty ADD CONSTRAINT fk_dish_counterparty FOREIGN KEY (COUNTERPARTY_id) REFERENCES counterparty(idCOUNTERPARTY);
ALTER TABLE dish_has_counterparty ADD CONSTRAINT fk_dish_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);

ALTER TABLE image ADD CONSTRAINT fk_image_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
ALTER TABLE image ADD CONSTRAINT fk_image_recipe FOREIGN KEY (RECIPE_id) REFERENCES recipe(idRECIPE);

ALTER TABLE ingredient ADD CONSTRAINT fk_ingredient_recipe FOREIGN KEY (RECIPE_id) REFERENCES recipe(idRECIPE);

ALTER TABLE person ADD CONSTRAINT fk_person_image FOREIGN KEY (IMAGE_id) REFERENCES image(idIMAGE);

ALTER TABLE person_has_note ADD CONSTRAINT fk_person_note_votant FOREIGN KEY (idPERSONVotant) REFERENCES person(idPERSON);
ALTER TABLE person_has_note ADD CONSTRAINT fk_person_note_votee FOREIGN KEY (idPERSONVotee) REFERENCES person(idPERSON);

ALTER TABLE person_has_address ADD CONSTRAINT fk_person_address FOREIGN KEY (ADDRESS_id) REFERENCES address(idADDRESS);
ALTER TABLE person_has_address ADD CONSTRAINT fk_person_person FOREIGN KEY (PERSON_id) REFERENCES person(idPERSON);

ALTER TABLE person_has_favorite ADD CONSTRAINT fk_person_person_like FOREIGN KEY (idPERSONLike) REFERENCES person(idPERSON);
ALTER TABLE person_has_favorite ADD CONSTRAINT fk_person_person_favorite FOREIGN KEY (idPERSONFavorite) REFERENCES person(idPERSON);

ALTER TABLE recipe ADD CONSTRAINT fk_recipe_person FOREIGN KEY (PERSON_id) REFERENCES person(idPERSON);
ALTER TABLE time ADD CONSTRAINT fk_time_dish FOREIGN KEY (DISH_id) REFERENCES dish(idDISH);
